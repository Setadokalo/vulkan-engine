#version 450
#define MAX_LIGHTS 4

struct light {
	vec3 position;
	vec3 color;
};

layout(location = 0) in  vec3 position;
layout(location = 1) in  vec2 uv;
layout(location = 2) in  vec3 normal;
layout(location = 3) in  vec3 tangent;
layout(location = 4) in  vec3 bitangent;

// Light color / count will change MUCH less than light position, so it gets it's own uniform identifier
// light color and count can also be reused in the fragment shader without transformation
layout(set = 0, binding = 0) uniform light_info {
	uint light_count;
	vec4[MAX_LIGHTS] light_colors;
};
layout(set = 0, binding = 1) uniform light_pos {
	vec4[MAX_LIGHTS] light_positions;
};
// This shader is only guaranteed to work on Vulkan but my shader linter yells mean things at me
// if I don't do this check :(
#ifdef VULKAN
layout(set = 1, binding = 0) uniform matrices {
#else
layout(binding = 2) uniform matrices {
#endif
	mat4 model;
	mat4 view;
	mat4 projection;
	mat4 mvp;
	mat3 mv3x3_matrix;
};


layout(location = 0) out vec2 f_uv;
layout(location = 1) out vec3 f_model_normal;
layout(location = 2) out vec3 eye_dir_tanspace;
layout(location = 3) out vec3 f_tangent;
layout(location = 4) out vec3 f_bitangent;
layout(location = 5) out vec3[MAX_LIGHTS] f_light_directions;

void main() {
	vec3 normal_camspace = (view * model * vec4(normalize(normal), 0.0)).xyz;
	vec3 tangent_camspace = (view * model * vec4(normalize(tangent), 0.0)).xyz;
	vec3 bitangent_camspace = (view * model * vec4(normalize(bitangent), 0.0)).xyz;
	f_tangent = tangent_camspace;
	f_bitangent = bitangent_camspace;

	mat3 TBN = transpose(mat3(
		tangent_camspace,
		bitangent_camspace,
		normal_camspace
	));

	vec3 vertex_pos_camspace = (view * model * vec4(position, 1)).xyz;

	vec3 eye_dir_camspace = -vertex_pos_camspace;
	eye_dir_tanspace = eye_dir_camspace;

	uint loop_max = min(MAX_LIGHTS, light_count);

	for (uint i = 0; i < loop_max; i++) {
		vec3 l_position = (view * vec4(light_positions[i].xyz, 1.0)).xyz;
		vec3 light_dir_camspace = l_position + eye_dir_camspace;
		f_light_directions[i] = TBN * light_dir_camspace;
	}
	// for (uint i = light_count; i < MAX_LIGHTS; i++) {
	// 	f_lights_direction[i] = vec3(0.0);
	// 	f_lights_color[i] = vec3(0.0);
	// 	f_lights_strength[i] = 0.0;
	// }
		
	f_model_normal  = normalize(normal_camspace);
	
	f_uv = uv;
	gl_Position = mvp * vec4(position, 1);
}