#version 450
#define MAX_LIGHTS 4

layout(location = 0) out vec4 color;

layout(location = 0) in  vec2 f_uv;
layout(location = 1) in  vec3 model_normal;
layout(location = 2) in  vec3 eye_dir_tanspace;
layout(location = 3) in  vec3 tangent;
layout(location = 4) in  vec3 bitangent;

layout(location = 5) in  vec3[MAX_LIGHTS] light_directions;


layout(binding = 1) uniform light_info {
	uint light_count;
	vec4[MAX_LIGHTS] light_colors;
};
layout(binding = 2) uniform sampler2D albedo_texture;
layout(binding = 3) uniform sampler2D normal_texture;
layout(binding = 4) uniform frag_info {
	vec2 pos;
	vec2 scale;
	uint debug_mode;
};



void main() {
	vec2 uv = (f_uv * scale) + pos;
	vec3 normal = normalize(texture(normal_texture, uv).rgb * 2.0 - 1.0);
	vec3 albedo = texture(albedo_texture, uv).rgb;

	vec3 net_color = vec3(0.0, 0.0, 0.0);
	// ambient light
	net_color.rgb = net_color.rgb + (albedo * 0.2);

	// making sure we don't overrun our buffer
	uint loop_max = min(MAX_LIGHTS, light_count);

	for (uint i = 0; i < loop_max; i++) {
		vec3  light_vec = light_directions[i];
		float light_distance = length(light_vec);
		vec3  light_dir = normalize(light_vec);

		float cosTheta = clamp(dot(normal, light_dir), 0,1);
		net_color.rgb += (1.0 / pow(light_distance, 2.0)) * cosTheta * light_colors[i].rgb * albedo;
	}
	
	color = vec4(net_color, 1.0);
	if (debug_mode == 1)
		color = vec4(model_normal, 1.0);
	else if (debug_mode == 2) {
		color.r = uv.x;
		color.g = -uv.y;
		color.b = 0.0;
	} else if (debug_mode == 3)
		color = vec4(tangent, 1.0);
	else if (debug_mode == 4)
		color = vec4(bitangent, 1.0);
	else if (debug_mode == 5)
		color = vec4(light_directions[0], 1.0);
	// color.rgb = model_normal;
}