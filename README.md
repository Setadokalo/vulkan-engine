A simple vulkan engine I'm writing in rust for the hell of it. I have no idea how hard my code is to read, so enter at your own risk.

# Cloning
If you clone this repo, make sure to use the `--recursive` flag to pull the submodules as well. If you forget, you can also run the command `git submodule update --init --recursive`.