use super::math_types::Quaternion;

pub fn is_power_of_two(x: u32) -> bool {
	(x != 0) && (x & (x - 1) == 0)
}

pub fn clamp(val: f32, min: f32, max: f32) -> f32 {
	if val < min {
		min
	} else if val > max {
		max
	} else {
		val
	}
}

pub fn wrap(val: f32, div: f32) -> f32 {
	(val / div).fract() * div
}

pub fn default_quat() -> Quaternion {
	Quaternion::new(1.0, 0.0, 0.0, 0.0)
}
