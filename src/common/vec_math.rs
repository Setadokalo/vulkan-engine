#![allow(dead_code)]
use super::math_types::{Quaternion, Vector2, Vector3, Vector4};

pub fn add_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] + b[0], a[1] + b[1], a[2] + b[2]]
}

pub fn sub_vec3(a: [f32; 3], b: [f32; 3]) -> [f32; 3] {
	[a[0] - b[0], a[1] - b[1], a[2] - b[2]]
}
pub fn div_vec3_scalar(a: [f32; 3], b: f32) -> [f32; 3] {
	[a[0] / b, a[1] / b, a[2] / b]
}
pub trait VectorMath {
	fn normalized(&self) -> Self;
	fn length(&self) -> f32;
}

impl VectorMath for Vector2 {
	fn normalized(&self) -> Self {
		let len = self.length();
		Vector2::new(self.x / len, self.y / len)
	}
	fn length(&self) -> f32 {
		(self.x.powi(2) + self.y.powi(2)).sqrt()
	}
}
pub trait ThreeDMath {
	fn dot(&self, other: Self) -> f32;
}
impl ThreeDMath for Vector3 {
	fn dot(&self, other: Self) -> f32 {
		(self.x * other.x) + (self.y * other.y) + (self.z * other.z)
	}
}

impl VectorMath for Vector3 {
	fn normalized(&self) -> Self {
		let len = self.length();
		assert!(len > 0.0);
		Vector3::new(self.x / len, self.y / len, self.z / len)
	}
	fn length(&self) -> f32 {
		(self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
	}
}
impl VectorMath for Vector4 {
	fn normalized(&self) -> Self {
		let len = self.length();
		Vector4::new(self.x / len, self.y / len, self.z / len, self.w / len)
	}
	fn length(&self) -> f32 {
		(self.x.powi(2) + self.y.powi(2) + self.z.powi(2) + self.w.powi(2)).sqrt()
	}
}

pub trait QuaternionMath {
	fn true_product(&self, other: &Self) -> Self;
}

impl QuaternionMath for Quaternion {
	fn true_product(&self, other: &Self) -> Self {
		Quaternion::new(
			(self.s * other.s)
				- (self.v.x * other.v.x)
				- (self.v.y * other.v.y)
				- (self.v.z * other.v.z),
			(self.s * other.v.x)
				- (self.v.x * other.s)
				- (self.v.y * other.v.z)
				- (self.v.z * other.v.y),
			(self.s * other.v.y)
				- (self.v.x * other.v.z)
				- (self.v.y * other.s)
				- (self.v.z * other.v.x),
			(self.s * other.v.z)
				- (self.v.x * other.v.y)
				- (self.v.y * other.v.x)
				- (self.v.z * other.s),
		)
	}
}
