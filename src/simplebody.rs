pub mod simple_meshinstance;
pub mod simple_physicsbody;

use crate::{
	common::{
		math_types::{Quaternion, Vector3},
		TwoWay,
	},
	asset_loading::mesh_instance::{PbrRenderer, RenderObjectMessage},
	physics::{physics_object::PhysicsObjectMessage, physics_server::PhysicsServerMessage},
	render_server::message::RenderServerMessage,
};
use simple_meshinstance::SimpleMeshInstance;
use simple_physicsbody::SimplePhysicsBody;
use std::sync::{
	mpsc::{self, Sender},
	Arc,
};
use vulkano::device::Queue;
//TODO: Remove this allow once the logic server is implemented (and thus SimpleObjects are used for more than just easier instantiation)
#[allow(dead_code)]
pub struct SimpleObject {
	physics_body: Sender<PhysicsObjectMessage>,
	mesh: Sender<RenderObjectMessage>,
}

impl SimpleObject {
	// generates a mesh and physics body, sends them to the appropriate servers, and stores a pipeline for sending each data
	pub fn new(
		ren_server_sender: &Sender<RenderServerMessage>,
		phy_server_sender: &Sender<PhysicsServerMessage>,
		queue: Arc<Queue>,
		model_path: String,
		physics_model_path: String,
		position: Vector3,
		rotation: Quaternion,
	) -> Self {
		let (mesh_sender, mesh_receiver) = mpsc::channel();
		let (physics_sender, physics_receiver) = mpsc::channel();

		let ren_messenger = TwoWay::new(physics_sender.clone(), mesh_receiver);
		let mesh = Box::new(PbrRenderer::new(SimpleMeshInstance::new(
			ren_messenger,
			model_path,
			queue,
			position,
			rotation,
		)));
		ren_server_sender
			.send(RenderServerMessage::AddObject(mesh))
			.expect("Failed to add object to render server");

		let phy_body_messager = TwoWay::new(mesh_sender.clone(), physics_receiver);
		let physics_body = Box::new(SimplePhysicsBody::new(
			physics_model_path,
			phy_body_messager,
			position,
			rotation,
		));
		phy_server_sender
			.send(PhysicsServerMessage::AddObject(physics_body))
			.expect("Failed to add object to physics server");
		Self {
			mesh: mesh_sender,
			physics_body: physics_sender,
		}
	}
}
