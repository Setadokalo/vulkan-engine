use input_map::InputMap;

use super::physics_server::PhysicsServerMessage;
use crate::common::math_types::*;
use std::sync::RwLockReadGuard;

//TODO: Remove this allow when the logic server is implemented
#[allow(dead_code)]
pub enum PhysicsObjectMessage {
	UpdatePosition(Vector3),
	UpdateRotation(Quaternion),
	SetProcess(bool),
}
unsafe impl Send for PhysicsObjectMessage {}
unsafe impl Sync for PhysicsObjectMessage {}

pub trait PhysicsObject {
	fn get_position(&self) -> Vector3;
	fn get_rotation(&self) -> Quaternion;
	fn step_physics(
		&mut self,
		index: usize,
		delta: f32,
		input_map: &RwLockReadGuard<InputMap>,
	) -> PhysicsServerMessage;
}
