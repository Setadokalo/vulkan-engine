use input_map::InputMap;

use super::physics_object::PhysicsObject;

use std::{
	rc::Rc,
	sync::{mpsc::Receiver, RwLockReadGuard}, time::Instant,
};
use crate::common::ExtendedBarrier;

pub enum PhysicsServerMessage {
	None,
	AddObject(Box<dyn PhysicsObject + Send>),
	// SendObjectMessage(usize, PhysicsObjectMessage),
	RemoveObject(usize),
	TerminateServer,
	PauseServer(bool),
}

pub struct PhysicsServer {
	objects: Vec<Box<dyn PhysicsObject + Send>>,
	receiver: Rc<Receiver<PhysicsServerMessage>>,
	continue_execution: bool,
	paused: bool,
}

impl PhysicsServer {
	pub fn new(
		receiver: Receiver<PhysicsServerMessage>
	) -> PhysicsServer {
		PhysicsServer {
			objects: Vec::new(),
			receiver: Rc::new(receiver),
			continue_execution: true,
			paused: false,
		}
	}
	/// Steps each object, then checks if the object returned a meaningful `PhysicsServerMessage`.
	pub fn step_simulation(&mut self, input_map: RwLockReadGuard<InputMap>, delta: f32) {
		let mut i = 0;
		while i < self.objects.len() {
			if let Some(obj) = self.objects.get_mut(i) {
				//TODO: Might be better for this to use a specialized type of message system so we don't have to panic at invalid indices
				//      Alternatively, we could do *shudders* MATH to ensure the loop continues safely
				let message = obj.step_physics(i, delta, &input_map);
				use PhysicsServerMessage::*;
				match message {
					AddObject(object) => {
						self.objects.push(object);
					}
					// SendObjectMessage(_, _) => {}
					RemoveObject(index) => {
						if i != index {
							panic!("Cannot remove element other than self in simulation step loop; try sending message through proper physics server channel");
						}
						// TODO: This is dangerous; we're iterating over the very list we remove an element from.
						self.objects.remove(index);
						continue; // Prevents incrementing the pointer, since the current element has been removed from the list
					}
					TerminateServer => {
						self.continue_execution = false;
					}
					PauseServer(pause) => {
						self.paused = pause;
					}
					None => {}
				}
			} else {
				panic!("Failed to get obj that was in bounds?!?!")
			}
			i += 1;
		}
	}

	pub fn poll_updates(&mut self) {
		let receiver = self.receiver.clone();
		for message in receiver.try_iter() {
			use PhysicsServerMessage::*;
			match message {
				AddObject(object) => {
					self.add_object(object);
				}
				// SendObjectMessage(_, _) => {}
				RemoveObject(index) => {
					self.objects.remove(index);
				}
				TerminateServer => {
					self.continue_execution = false;
				}
				PauseServer(pause) => {
					self.paused = pause;
				}
				None => {}
			}
		}
	}

	pub fn run_server(mut self, mut barrier: ExtendedBarrier) {
		// Physics server uses it's own timing
		// let mut timing = Timing::new(60, 120.0, false);
		let mut instant = Instant::now();

		while self.continue_execution {
			// timing.sleep_remaining();
			barrier.wait();
			let delta = instant.elapsed().as_secs_f32();
			instant = Instant::now();
			self.poll_updates();
			if !self.paused {
				self.step_simulation(barrier.get_input_map(), delta);
			}
		}
	}

	pub fn add_object(&mut self, object: Box<dyn PhysicsObject + Send>) {
		self.objects.push(object);
	}
}
