use crate::{
	common::math_types::*, asset_loading::mesh_instance::RenderObjectMessage,
	render_server::render_camera::RenderServerCamera,
};
use std::sync::mpsc::{self, Receiver, Sender};

pub struct RenderCamera {
	position: Vector3,
	rotation: Quaternion,
	recv: Receiver<RenderObjectMessage>,
}
impl RenderCamera {
	pub fn new(
		position: Vector3,
		rotation: Quaternion,
	) -> (RenderCamera, Sender<RenderObjectMessage>) {
		let (sen, recv) = mpsc::channel();
		(
			RenderCamera {
				position,
				rotation,
				recv,
			},
			sen,
		)
	}
}

impl RenderServerCamera for RenderCamera {
	fn get_position(&self) -> Vector3 {
		self.position
	}

	fn get_rotation(&self) -> Quaternion {
		self.rotation
	}

	fn poll_messages(&mut self) {
		for message in self.recv.try_iter() {
			match message {
				RenderObjectMessage::UpdatePosition(position) => {
					self.position = position;
				}
				RenderObjectMessage::UpdateRotation(rotation) => {
					//  println!("Received UpdateRotation message; setting rotation from {:?} to {:?}", self.rotation, rotation);
					self.rotation = rotation;
				}
			}
		}
	}
}
