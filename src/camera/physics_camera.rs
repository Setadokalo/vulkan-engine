use crate::{
	common::{math_types::*, vec_math::VectorMath, TwoWay},
	asset_loading::mesh_instance::RenderObjectMessage,
	physics::{
		physics_object::{PhysicsObject, PhysicsObjectMessage},
		physics_server::PhysicsServerMessage,
	},
	MOVE_SPEED,
};
use cgmath::Euler;
use input_map::InputMap;
use std::sync::{
	mpsc::{self, Sender}, RwLockReadGuard,
};

const MOUSE_SENS: f32 = 0.25;
pub struct PhysicsCamera {
	comm: TwoWay<PhysicsObjectMessage, RenderObjectMessage>,
	position: Vector3,
	rotation: Quaternion,
	velocity: Vector3,
}

impl PhysicsCamera {
	pub fn new(
		sender: Sender<RenderObjectMessage>,
		position: Vector3,
		rotation: Quaternion,
	) -> (Self, Sender<PhysicsObjectMessage>) {
		let (phy_sender, receiver) = mpsc::channel();
		let comm = TwoWay::new(sender, receiver);
		(
			Self {
				comm,
				position,
				rotation,
				velocity: Vector3::new(0.0, 0.0, 0.0),
			},
			phy_sender,
		)
	}
}

impl PhysicsObject for PhysicsCamera {
	fn get_position(&self) -> Vector3 {
		self.position
	}

	fn get_rotation(&self) -> Quaternion {
		self.rotation
	}

	fn step_physics(
		&mut self,
		_index: usize,
		delta: f32,
		input_map: &RwLockReadGuard<InputMap>,
	) -> PhysicsServerMessage {
		let mut player_input = Vector3::new(0.0, 0.0, 0.0);
		let mut vert_player_input = Vector3::new(0.0, 0.0, 0.0);

		let start_pos = self.get_position();
		let start_rot = self.get_rotation();

		if input_map.is_action_pressed("move_left") {
			player_input.x = player_input.x + 1.0;
		}
		if input_map.is_action_pressed("move_right") {
			player_input.x = player_input.x - 1.0;
		}
		if input_map.is_action_pressed("move_forward") {
			player_input.z = player_input.z - 1.0;
		}
		if input_map.is_action_pressed("move_backward") {
			player_input.z = player_input.z + 1.0;
		}
		if player_input.length() > 0.0 {
			player_input = player_input.normalized();
		}

		if input_map.is_action_pressed("move_down") {
			vert_player_input.y = vert_player_input.y - 1.0;
		}
		if input_map.is_action_pressed("move_up") {
			vert_player_input.y = vert_player_input.y + 1.0;
		}

		if player_input.length() > 0.0 {
			// flat_cam_rot.x = cgmath::Rad(0.0);

			let mut rotated_player_input: Vector3 = self.get_rotation() * player_input;
			rotated_player_input.y = 0.0;
			rotated_player_input = rotated_player_input.normalized();
			rotated_player_input.x = -rotated_player_input.x;
			self.velocity = self.velocity + (rotated_player_input * MOVE_SPEED);
		}
		self.velocity = self.velocity + (vert_player_input * MOVE_SPEED);
		self.velocity = self.velocity * 0.98;

		self.position += self.velocity * (delta as f32);

		let player_mouse_motion = input_map.get_mouse_motion();
		let mut cam_euler = Euler::new(
			cgmath::Deg(0.0f32),
			cgmath::Deg(0.0f32),
			cgmath::Deg(0.0f32),
		);
		cam_euler.y = cgmath::Deg(player_mouse_motion.x * MOUSE_SENS);
		self.rotation = self.rotation * Quaternion::from(cam_euler);
		cam_euler.x = cgmath::Deg(player_mouse_motion.y * MOUSE_SENS);
		cam_euler.y = cgmath::Deg(0.0);
		cam_euler.z = cgmath::Deg(0.0);
		self.rotation = Quaternion::from(cam_euler) * self.rotation;

		if start_pos != self.get_position() {
			self
				.comm
				.sender
				.send(RenderObjectMessage::UpdatePosition(self.get_position()))
				.expect("Failed to update position");
		}
		if start_rot != self.get_rotation() {
			self
				.comm
				.sender
				.send(RenderObjectMessage::UpdateRotation(self.get_rotation()))
				.expect("Failed to update position");
		}
		PhysicsServerMessage::None
	}
}
