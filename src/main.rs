use event::{DeviceEvent, VirtualKeyCode, WindowEvent};
use std::{
	sync::{mpsc, Arc},
	thread,
};
use vulkano::{device::Queue, instance::Instance};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::{
	dpi::{LogicalSize, Size},
	event::{self, Event},
};

mod camera;
mod common;
mod asset_loading;
mod physics;
mod render_server;
mod simplebody;

use camera::Camera;

use common::{RootExtendedBarrier, math_types::{Quaternion, Vector2, Vector3}};
use frame_timing::Timing;

use mpsc::Sender;
use physics::physics_server::{PhysicsServer, PhysicsServerMessage};
use render_server::{message::RenderServerMessage, RenderServer, Light};

use simplebody::SimpleObject;
use input_map::{InputMap, ActionKey};

extern crate shaders;

static MIN_INNER_SIZE: Size = Size::Logical(LogicalSize::new(256.0, 256.0));
const MOVE_SPEED: f32 = 0.5;

fn add_objects(
	queue: Arc<Queue>,
	ren_send: &Sender<RenderServerMessage>,
	phy_send: &Sender<PhysicsServerMessage>,
) {
	// let mut rng_gen: StdRng = StdRng::from_entropy();
	SimpleObject::new(
		ren_send,
		phy_send,
		queue.clone(),
		String::from("assets/models/newell_teaset/teapot_upgrade.obj"),
		String::from("assets/models/newell_teaset/teapot_upgrade.obj"),
		Vector3::new(-2.5, -1.0, 0.0),
		Quaternion::new(1.0, 0.0, 0.0, 0.0),
	);

	println!("Setting up second teapot");

	SimpleObject::new(
		ren_send,
		phy_send,
		queue.clone(),
		String::from("assets/models/newell_teaset/teapot_upgrade_simpl.obj"),
		String::from("assets/models/newell_teaset/teapot_upgrade_simpl.obj"),
		Vector3::new(2.5, -1.0, 0.0),
		Quaternion::new(1.0, 0.0, 0.0, 0.0),
	);
	Camera::new(ren_send, phy_send, Vector3::new(0.0, 0.0, 0.0), None);
}

fn main() {
	let instance = {
		let extensions = vulkano_win::required_extensions();
		Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
	};
	let events_loop = EventLoop::new();
	let control_config = std::fs::read_to_string("config/controls.ron").expect("Failed to read controls config");
	// let input_map_lock = Arc::new(RwLock::new());
	let mut input_map: InputMap = ron::from_str(&control_config).expect("Failed to deserialize config");

	// input map scope
	input_map.weak_create_action("toggle_antialias", ActionKey::VirtualId(VirtualKeyCode::Return));
	input_map.weak_create_action("fullscreen", 
		ActionKey::Combo(
			vec![
				ActionKey::VirtualId(VirtualKeyCode::LShift),
				ActionKey::VirtualId(VirtualKeyCode::Return),
			]
		)
	);
	input_map.weak_create_action("quit", ActionKey::VirtualId(VirtualKeyCode::Q));
	input_map.weak_create_action_vec(
		"pause",
		vec![
			ActionKey::VirtualId(VirtualKeyCode::P),
			ActionKey::VirtualId(VirtualKeyCode::Escape),
		],
	);

	input_map.weak_create_action("move_forward", ActionKey::VirtualId(VirtualKeyCode::W));
	input_map.weak_create_action("move_backward", ActionKey::VirtualId(VirtualKeyCode::S));
	input_map.weak_create_action("move_left", ActionKey::VirtualId(VirtualKeyCode::A));
	input_map.weak_create_action("move_right", ActionKey::VirtualId(VirtualKeyCode::D));
	input_map.weak_create_action("move_up", ActionKey::VirtualId(VirtualKeyCode::Space));
	input_map.weak_create_action("move_down", ActionKey::VirtualId(VirtualKeyCode::LControl));

	input_map.weak_create_action("debug_mode_0", ActionKey::VirtualId(VirtualKeyCode::Key1));
	input_map.weak_create_action("debug_mode_1", ActionKey::VirtualId(VirtualKeyCode::Key2));
	input_map.weak_create_action("debug_mode_2", ActionKey::VirtualId(VirtualKeyCode::Key3));
	input_map.weak_create_action("debug_mode_3", ActionKey::VirtualId(VirtualKeyCode::Key4));
	input_map.weak_create_action("debug_mode_4", ActionKey::VirtualId(VirtualKeyCode::Key5));
	input_map.weak_create_action("debug_mode_5", ActionKey::VirtualId(VirtualKeyCode::Key6));

	let mut barrier = RootExtendedBarrier::new(true, input_map);

	let (mut render_server, limits, render_sender) = 
		RenderServer::new(instance, &events_loop);
	let (physics_sender, physics_receiver) = mpsc::channel();

	{
		let phy_barrier = barrier.clone_builder();
		thread::spawn(move || {
			let physics_server = PhysicsServer::new(physics_receiver);
			let phy_barrier = phy_barrier.build();
			physics_server.run_server(phy_barrier);
		});
	}

	// let async_executor_pool: ThreadPool = ThreadPool::builder().pool_size(1).create().expect("Failed to build async pool");

	add_objects(render_server.queue.clone(), &render_sender, &physics_sender);


	render_server
		.add_light(
			Light {
			   color: [1.0; 4],
			   position: [0.0, 2.0, 2.0, 1.0],
			}
		)
		.expect("Failed to add light");
	// target framerate *ever so slightly* higher than the real target to get slightly closer to consistent target framerate
	let mut timing = Timing::new(60, 120.25, true);

	let mut antialiasing = true;
	// timing.step_timing();
	let mut player_motion = Vector3::new(0.0, 0.0, 0.0);
	let mut physics_paused = false;

	println!("Entering event loop!");
	let mut player_mouse_motion = cgmath::Vector2::new(0.0_f64, 0.0_f64);
	let mut key_buffer = Vec::new();
	events_loop.run(move |event, _, control_flow| {
		match event {
			Event::WindowEvent {
				event: event::WindowEvent::CloseRequested,
				..
			} => {
				*control_flow = ControlFlow::Exit;
			}
			Event::WindowEvent {
				event: WindowEvent::Resized(_),
				..
			} => {
				render_sender
					.send(RenderServerMessage::InvalidateSwapchain)
					.unwrap();
			}
			Event::WindowEvent {
				event: WindowEvent::Focused(focus),
				..
			} => {
				physics_sender
					.send(PhysicsServerMessage::PauseServer(physics_paused || !focus))
					.unwrap();
				render_sender
					.send(RenderServerMessage::SetMouseGrab(
						!(physics_paused || !focus),
					))
					.unwrap();
			}
			Event::WindowEvent {
				event: WindowEvent::KeyboardInput { input, .. },
				..
			} => {
				// TODO: update `update_keys` method to match new InputMap control scheme
				key_buffer.push(input);
			}
			Event::WindowEvent {
				event: event::WindowEvent::CursorMoved { position, .. },
				..
			} => {
				render_sender
					.send(RenderServerMessage::UpdateMousePosition(position))
					.unwrap();
			}
			event::Event::RedrawEventsCleared => {}
			event::Event::MainEventsCleared => {
				// we do this at the start of the loop instead of the end to maintain a consistent framerate regardless of the other events we need to process
				// (like keyboard input)
				barrier.wait_with_callback(|mut input_map_access| {
					input_map_access.step_frame();
					for input in key_buffer.iter() {
						input_map_access.update_keys(input.clone());
					}
					key_buffer.clear();
					input_map_access.add_mouse_motion(Vector2::new(
						player_mouse_motion.x as f32,
						player_mouse_motion.y as f32,
					));
					render_server.inter_frame_step();
				});
				timing.sleep_remaining();
				let delta = timing.step_timing();

				// air resistance, essentially
				player_motion *= 0.96;

				// input map scope
				{
					player_mouse_motion.x = 0.0;
					player_mouse_motion.y = 0.0;
					let input_map = barrier.get_input_map();
					if input_map.is_action_just_pressed("toggle_antialias") {
						render_server
							.set_msaa_level(if antialiasing {
								1
							} else {
								limits.max_msaa_level
							})
							.expect("failed to change antialiasing mode");
						antialiasing = !antialiasing;
					}
					if input_map.is_action_just_pressed("quit") {
						*control_flow = ControlFlow::Exit;
					}
					if input_map.is_action_just_pressed("pause") {
						physics_sender
							.send(PhysicsServerMessage::PauseServer(!physics_paused))
							.unwrap();
						render_sender
							.send(RenderServerMessage::SetMouseGrab(physics_paused))
							.unwrap();
						physics_paused = !physics_paused;
					}

					let debug_mode = if input_map.is_action_pressed("debug_mode_0") {
						Some(0)
					} else if input_map.is_action_pressed("debug_mode_1") {
						Some(1)
					} else if input_map.is_action_pressed("debug_mode_2") {
						Some(2)
					} else if input_map.is_action_pressed("debug_mode_3") {
						Some(3)
					} else if input_map.is_action_pressed("debug_mode_4") {
						Some(4)
					} else if input_map.is_action_pressed("debug_mode_5") {
						Some(5)
					} else {
						None
					};
					if let Some(mode) = debug_mode {
						render_sender
							.send(RenderServerMessage::SetDebugMode(mode))
							.unwrap();
					}
				}
				render_server.poll_messages();
				render_server.draw_frame(delta);
			}
			event::Event::LoopDestroyed => {
				physics_sender
					.send(PhysicsServerMessage::TerminateServer)
					.unwrap();
				std::fs::write(
					"config/controls.ron", 
					ron::ser::to_string_pretty(
						&*barrier.get_input_map(),
						Default::default(),
					).expect("Failed to serialize controls config!")
				).expect("Failed to write controls config to disk");
			}
			event::Event::DeviceEvent {
				device_id: _device_id,
				event,
			} => {
				if let DeviceEvent::MouseMotion { delta } = event {
					player_mouse_motion += cgmath::Vector2::new(delta.0, delta.1);
				}
			}
			_ => {}
		}
	});
}
