pub mod math_types {
	pub type Vec3 = [f32; 3];
	pub type Vec2 = [f32; 2];

	pub type Matrix3 = cgmath::Matrix3<f32>;
	pub type Matrix4 = cgmath::Matrix4<f32>;

	pub type Vector2 = cgmath::Vector2<f32>;
	pub type Vector3 = cgmath::Vector3<f32>;
	pub type Vector4 = cgmath::Vector4<f32>;

	pub type Rad = cgmath::Rad<f32>;
	// pub type Euler = cgmath::Euler<f32>;
	pub type Quaternion = cgmath::Quaternion<f32>;
}
use math_types::*;

#[allow(dead_code)]
pub mod math;
pub mod vec_math;

use std::{
	fmt::Debug,
	sync::{Arc, mpsc::{Receiver, RecvError, SendError, Sender, TryRecvError, TryIter}, RwLock, RwLockReadGuard, RwLockWriteGuard},
};
use vec_math::*;
use extended_barrier::Barrier;
use input_map::InputMap;

#[derive(Default, Debug, Copy, Clone)]
pub struct Vertex {
	pub position: Vec3,
	pub uv: Vec2,
	pub normal: Vec3,
	pub tangent: Vec3,
	pub bitangent: Vec3,
}
impl std::cmp::PartialEq for Vertex {
	// tangent and bitangent are ignored because they are merely properties for equivalent vertices to share
	fn eq(&self, other: &Self) -> bool {
		for i in 0..3 {
			if self.position[i] != other.position[i] {
				return false;
			}
			if self.normal[i] != other.normal[i] {
				return false;
			}
		}
		for i in 0..2 {
			if self.uv[i] != other.uv[i] {
				return false;
			}
		}
		return true;
	}
}

impl Vertex {
	pub fn new(
		position: Vec3,
		uv: Option<Vec2>,
		normal: Option<Vec3>,
		tan: Option<Vec3>,
		bitan: Option<Vec3>,
	) -> Vertex {
		Vertex {
			position,
			uv: uv.unwrap_or([0.0_f32, 0.0]),
			normal: normal.unwrap_or([0.0_f32, 1.0, 0.0]),
			tangent: tan.unwrap_or([1.0_f32, 0.0, 0.0]),
			bitangent: bitan.unwrap_or([0.0_f32, 0.0, 1.0]),
		}
	}
	pub fn join(self, other: Vertex) -> Vertex {
		let mut ret = self;
		ret.tangent = add_vec3(
			ret.tangent,
			div_vec3_scalar(sub_vec3(self.tangent, other.tangent), 2.0),
		);
		ret.bitangent = add_vec3(
			ret.bitangent,
			div_vec3_scalar(sub_vec3(self.bitangent, other.bitangent), 2.0),
		);
		ret
	}
}

vulkano::impl_vertex!(Vertex, position, uv, normal, tangent, bitangent);

pub fn get_transformation_matrix(
	scale: Vector3,
	translation: Vector3,
	rotation: Quaternion,
) -> Matrix4 {
	let scale_mat = Matrix4::from_nonuniform_scale(scale.x, scale.y, scale.z);
	let trans_mat = Matrix4::from_translation(translation);
	let rot_mat = Matrix4::from(rotation);
	trans_mat * rot_mat * scale_mat
}

pub fn extract_topleft_matrix(matrix: &Matrix4) -> Matrix3 {
	Matrix3::new(
		matrix.x[0],
		matrix.x[1],
		matrix.x[2],
		matrix.y[0],
		matrix.y[1],
		matrix.y[2],
		matrix.z[0],
		matrix.z[1],
		matrix.z[2],
	)
}

pub fn get_projection_matrix(fov: Rad, aspect_ratio: f32, near: f32, far: f32) -> Matrix4 {
	cgmath::Matrix4::from_nonuniform_scale(1.0, -1.0, 1.0)
		* cgmath::perspective(fov, aspect_ratio, near, far)
}

// helper function
pub fn arr_to_vec(pos: [f32; 3]) -> Vector3 {
	Vector3::from([pos[0], pos[1], pos[2]])
}

// pub fn matrix_to_array(mat: Matrix4) -> [[f32; 4]; 4] {
// 	[
// 		[mat.x[0], mat.x[1], mat.x[2], mat.x[3]],
// 		[mat.y[0], mat.y[1], mat.y[2], mat.y[3]],
// 		[mat.z[0], mat.z[1], mat.z[2], mat.z[3]],
// 		[mat.w[0], mat.w[1], mat.w[2], mat.w[3]],
// 	]
// }

#[derive(Debug)]
pub struct TwoWay<R, S> {
	pub sender: Sender<S>,
	pub receiver: Receiver<R>,
}
#[allow(dead_code)]
impl<R, S> TwoWay<R, S> {
	pub fn new(sender: Sender<S>, receiver: Receiver<R>) -> TwoWay<R, S> {
		TwoWay { sender, receiver }
	}
	pub fn send(&self, v: S) -> Result<(), SendError<S>> {
		self.sender.send(v)
	}
	pub fn try_recv(&self) -> Result<R, TryRecvError> {
		self.receiver.try_recv()
	}
	pub fn recv(&self) -> Result<R, RecvError> {
		self.receiver.recv()
	}
	pub fn try_iter(&self) -> TryIter<'_, R>{
		self.receiver.try_iter()
	}
}
unsafe impl<R: Send, S: Send> Send for TwoWay<R, S> {}

/// Provides a wrapper for `Barrier` that provides a space in between the previous frame and next frame to execute additional logic.
/// This is not restricted to game logic; Any environment where one needs a "before" and "after" state, and needs to execute logic in between
/// those states, can use this structure. An example: In between frames, one might clear frame state data from an input map, like "button just pressed".
#[derive(Debug)]
pub struct RootExtendedBarrier {
	barrier: Barrier,
	input_map_raw: Arc<RwLock<InputMap>>,
}
/// Provides a wrapper for `Barrier` that provides a space in between the previous frame and next frame to execute additional logic.
/// This is not restricted to game logic; Any environment where one needs a "before" and "after" state, and needs to execute logic in between
/// those states, can use this structure. An example: In between frames, one might clear frame state data from an input map, like "button just pressed".
#[derive(Debug)]
pub struct ExtendedBarrier {
	barrier: Barrier,
	input_map_raw: Arc<RwLock<InputMap>>,
}

pub struct ExtendedBarrierBuilder {
	barrier: Barrier,
	input_map_raw: Arc<RwLock<InputMap>>,
}

impl ExtendedBarrierBuilder {
	pub fn build(self) -> ExtendedBarrier {
		let ret = ExtendedBarrier {
			barrier: self.barrier,
			input_map_raw: self.input_map_raw.clone(),
		};
		ret
	}
}

#[allow(dead_code)]
impl RootExtendedBarrier {
	/// Waits for all threads to be ready at this barrier, calls the callback function, then waits for all threads to be ready again.
	/// Useful, for example, for running code in between frames.
	pub fn wait_with_callback<F: FnOnce(RwLockWriteGuard<InputMap>)>(&mut self, callback: F) {
		self.barrier.wait();
		callback(self.input_map_raw.write().expect("Failed to get write access for root barrier callback"));
		self.barrier.wait();
	}
	/// A convenience wrapper for `wait_with_callback` for threads that do not need to perform special logic in between frames.
	pub fn wait(&mut self) {
		self.wait_with_callback(|_| {});
	}

	pub fn threads(&self) -> usize {
		self.barrier.threads()
	}

	pub fn waiting_threads(&self) -> usize {
		self.barrier.waiting_threads()
	}

	/// Constructs a new ExtendedBarrier.
	pub fn new(use_yields: bool, input_map: InputMap) -> Self {
		let input_map_raw = Arc::new(RwLock::new(input_map));
		Self {
			barrier: Barrier::new(use_yields),
			input_map_raw,
		}
	}
	pub fn clone(&mut self) -> ExtendedBarrier {
		ExtendedBarrier {
		    barrier: self.barrier.clone(),
			 input_map_raw: self.input_map_raw.clone(),
		}
	}
	pub fn clone_builder(&mut self) -> ExtendedBarrierBuilder {
		ExtendedBarrierBuilder {
			barrier: self.barrier.clone(),
			input_map_raw: self.input_map_raw.clone(),
		}
	}
	pub fn get_input_map(&self) -> RwLockReadGuard<InputMap> {
		self.input_map_raw.read().expect("Mutex poisoned!")
	}
}

#[allow(dead_code)]
impl ExtendedBarrier {
	pub fn wait_with_callback<F: FnOnce()>(&mut self, callback: F) {
		self.barrier.wait();
		callback();
		self.barrier.wait();
	}
	/// A convenience wrapper for `wait_with_callback` for threads that do not need to perform special logic in between frames.
	pub fn wait(&mut self) {
		self.wait_with_callback(|| {});
	}

	pub fn threads(&self) -> usize {
		self.barrier.threads()
	}

	pub fn waiting_threads(&self) -> usize {
		self.barrier.waiting_threads()
	}
	pub fn get_input_map(&self) -> RwLockReadGuard<InputMap> {
		self.input_map_raw.read().expect("Mutex poisoned!")
	}
}
