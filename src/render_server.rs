use cgmath::Deg;
use message::RenderServerMessage;
use serde::{Deserialize, Serialize};

use std::{
	cmp::Ordering,
	error::Error,
	fmt::Debug,
	fmt::Display,
	fs::read_to_string,
	rc::Rc,
	sync::{
		mpsc::{self, Receiver, Sender},
		Arc,
	},
};
use swapchain::{AcquireError, SwapchainAcquireFuture};
use sync::FlushError;
use vulkano::{
	command_buffer::{
		pool::standard::StandardCommandPoolAlloc, AutoCommandBuffer, AutoCommandBufferBuilder,
		DynamicState,
	},
	device::{Device, Queue},
	format::{ClearValue, Format},
	framebuffer::{FramebufferAbstract, RenderPassAbstract},
	image::AttachmentImage,
	instance::{
		Instance, PhysicalDevice,
		PhysicalDeviceType::{DiscreteGpu, IntegratedGpu, VirtualGpu},
	},
	swapchain::{self, Capabilities, CompositeAlpha, Surface, Swapchain, SwapchainCreationError},
	sync::{self, GpuFuture},
};
use vulkano_win::VkSurfaceBuild;
use winit::{
	dpi::{PhysicalPosition, PhysicalSize, Size},
	event_loop::EventLoop,
	window::{Icon, Window, WindowBuilder},
};

pub mod message;
pub mod render_camera;
mod utility;

use crate::{
	common::{self, math::is_power_of_two, math_types::*},
	asset_loading::mesh_instance::Renderer,
	MIN_INNER_SIZE,
};
use render_camera::RenderServerCamera;
use utility::*;


#[derive(Debug)]
pub enum UpdateError<T> {
	UnsupportedValue(T),
	BufferFull,
}
impl<T> UpdateError<T>
where
	T: Debug,
{
	fn description(&self) -> String {
		use UpdateError::*;
		match self {
			UnsupportedValue(val) => format!("Unsupported value '{:?}'", val),
			BufferFull => "Buffer already full".into(),
		}
	}
}
impl<T> Display for UpdateError<T>
where
	T: Debug,
{
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Failed to update value: {}", self.description())
	}
}

impl<T> Error for UpdateError<T>
where
	T: Debug,
{
	fn source(&self) -> Option<&(dyn Error + 'static)> {
		None
	}
}

//TODO: add more device limits to this struct
pub struct DeviceLimits {
	pub max_msaa_level: u32,
	pub supported_msaa_levels: u32,
	pub capabilities: Capabilities,
}

#[derive(Serialize, Deserialize)]
pub struct VideoSettings {
	#[serde(skip)] // this variable is only used so we know *where* to re-serialize *to*; storing it on disk makes no sense
	path: Option<String>,
	#[serde(skip)]
	original_size: Option<(u32, u32)>,
	#[serde(default)]
	fullscreen: bool,
	#[serde(default = "default_size")]
	size: (u32, u32),
	#[serde(default = "one")]
	antialiasing: u32,
}

/// helper function to provide serde defaults for values that should never be 0.
fn one() -> u32 {
	1
}

fn default_size() -> (u32, u32) {
	(1000, 600)
}

impl VideoSettings {
	pub fn load_from_file(path: &str) -> VideoSettings {
		let raw_settings = read_to_string(path).expect("Failed to read settings file");
		let mut ret = Self::load_from_string(raw_settings);
		ret.path = Some(path.to_string());
		ret
	}
	pub fn load_from_string<T: Into<String>>(data: T) -> VideoSettings {
		let data: String = data.into();
		let mut ret: VideoSettings = ron::from_str(&*data).expect("Failed to deserialize settings string");
		ret.original_size = Some(ret.size);
		ret

	}
}
impl Drop for VideoSettings {
    fn drop(&mut self) {
		self.size = if let Some(s) = self.original_size {s} else {self.size};
		if let Some(path) = self.path.clone() {
			if let Ok(serialized) = ron::ser::to_string_pretty(self, Default::default()) {
				if let Err(err) = std::fs::write(path.clone(), serialized) {
					println!("WARNING: Failed to write settings to disk! Settings were not saved!");
					println!("Error was {:?}", err);
				}
			} else {
				println!("WARNING: Failed to convert settings to string! Settings were not saved!");
			}
		}
   }
	
}

struct ServerInterop<T> {
	receiver: Rc<Receiver<T>>,
}

pub struct Light {
	pub color: [f32; 4],
	pub position: [f32; 4],
}
impl Default for Light {
    fn default() -> Self {
        Self {
			  color: [1.0; 4],
			  position: [0.0; 4],
		  }
    }
	
}
impl Clone for Light {
    fn clone(&self) -> Self {
        Self {
			  color: self.color,
			  position: self.position
		  }
    }
	
}
impl Copy for Light {}

/// Represents a single window and all related structures and functions.
///
/// Essentially serves as an abstraction layer to make rendering easier
pub struct RenderServer {
	// interop with other servers
	server_interop: ServerInterop<RenderServerMessage>,
	//physical device info
	pub device: Arc<Device>,
	limits: Arc<DeviceLimits>,
	pub queue: Arc<Queue>,
	//window info
	pub surface: Arc<Surface<Window>>,
	pub aspect_ratio: f32,
	mouse_position: PhysicalPosition<f64>,
	//low-level rendering info
	dynamic_state: DynamicState,
	pub render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
	swapchain: Arc<Swapchain<Window>>,
	framebuffers: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
	format: Format,
	//internal flags
	recreate_swapchain: bool,
	previous_frame_end: Option<Box<dyn GpuFuture>>,
	pub debug_mode: u32,
	//mid-level rendering info
	settings: VideoSettings,
	alpha_mode: CompositeAlpha,
	// high-level rendering info
	pub objects: Vec<Box<dyn Renderer>>,
	pub cameras: Vec<Box<dyn RenderServerCamera>>,
	pub lights: [Light; 4],
	pub light_count: u32,
	pub projection_matrix: Matrix4,
	current_camera: usize,
}

impl<'a> RenderServer {
	//TODO: Further break this apart into smaller reusable functions
	pub fn new(
		instance: Arc<Instance>,
		events_loop: &EventLoop<()>,
	) -> (Self, Arc<DeviceLimits>, Sender<RenderServerMessage>) {
		let (icon, icon_dimensions) = load_icon();

		let physical: PhysicalDevice = PhysicalDevice::enumerate(&instance)
			.max_by(|&phy1, &phy2| match (phy1.ty(), phy2.ty()) {
				(DiscreteGpu, _) => Ordering::Greater,
				(_, DiscreteGpu) => Ordering::Less,
				(IntegratedGpu, _) => Ordering::Greater,
				(_, IntegratedGpu) => Ordering::Less,
				(VirtualGpu, _) => Ordering::Greater,
				(_, VirtualGpu) => Ordering::Less,
				_ => Ordering::Equal,
			})
			.expect("Failed to get GPU");

		println!("Chose device '{}'", physical.name());

		let surface = WindowBuilder::new()
			.with_min_inner_size(MIN_INNER_SIZE)
			// We start at the minimum window size and then resize to our normal size later
			// to prevent bugs with certain overlay tools
			.with_inner_size(MIN_INNER_SIZE)
			.with_title("Vulkan Experiment")
			.with_window_icon(Some(
				Icon::from_rgba(icon, icon_dimensions.0, icon_dimensions.1)
					.expect("Failed to build icon"),
			))
			.build_vk_surface(events_loop, instance.clone())
			.expect("Failed to build surface");

		surface.window().set_cursor_visible(false);

		let capabilities = surface
			.capabilities(physical)
			.expect("failed to get surface capabilities");

		let limits = DeviceLimits {
			max_msaa_level: get_max_supported_msaa_level(physical.clone()),
			supported_msaa_levels: get_supported_msaa_levels(physical.clone()),
			capabilities,
		};
		let (queue, device) = get_device_queue(physical);
		let dimensions = surface.window().inner_size().into();

		let settings = VideoSettings::load_from_file("config/display.ron");

		let format = limits.capabilities.supported_formats[0].0;
		let alpha_mode = get_supported_alpha(limits.capabilities.supported_composite_alpha);
		let mut dynamic_state = DynamicState::none();

		let (swapchain, images) = build_swapchain(
			device.clone(),
			surface.clone(),
			&limits.capabilities,
			format,
			dimensions,
			queue.clone(),
			get_supported_alpha(limits.capabilities.supported_composite_alpha),
		);
		let render_pass = build_render_pass(settings.antialiasing, device.clone(), format);
		let intermediary =
			AttachmentImage::transient_multisampled(device.clone(), dimensions, settings.antialiasing, format)
				.unwrap();
		let framebuffers = window_size_dependent_setup(
			&images,
			intermediary.clone(),
			render_pass.clone(),
			device.clone(),
			settings.antialiasing,
			&mut dynamic_state,
		);

		let aspect_ratio = dimensions[0] as f32 / dimensions[1] as f32;

		let (sender, receiver) = mpsc::channel();

		let mut ret = Self {
			server_interop: ServerInterop {
				receiver: Rc::new(receiver),
			},
			//physical device/window info
			device: device.clone(),
			limits: Arc::new(limits),
			queue: queue.clone(),
			//window info
			surface: surface.clone(),
			aspect_ratio,
			mouse_position: PhysicalPosition::new(0.0, 0.0),
			//low-level rendering info
			dynamic_state,
			render_pass,
			swapchain,
			framebuffers,
			format,
			//internal flags
			recreate_swapchain: true,
			previous_frame_end: Some(sync::now(device.clone()).boxed()),
			debug_mode: 0,
			//mid-level rendering info
			alpha_mode,
			settings,
			//high-level rendering data
			objects: Vec::new(),
			cameras: Vec::new(),
			lights: [Light::default(); 4],
			light_count: 0u32,
			projection_matrix: common::get_projection_matrix(
				Deg(90.0).into(),
				aspect_ratio,
				0.01,
				100.0,
			),
			current_camera: 0,
		};

		println!("Max sample count: {}", ret.settings.antialiasing);
		println!(
			"Supported formats: {:?}",
			ret.limits.capabilities.supported_formats
		);

		if let Err(e) = surface.window().set_cursor_grab(true) {
			println!("Warning: Failed to grab cursor! Reason: {:?}", e);
		};
		let lim = ret.limits.clone();
		ret.set_window_size(ret.settings.size);
		(ret, lim, sender)
	}

	/// Rebuilds the render pass for the struct using the stored settings.
	pub fn build_render_pass(&mut self) {
		self.render_pass =
			build_render_pass(self.settings.antialiasing, self.device.clone(), self.format.clone());
		for obj in self.objects.iter_mut() {
			(*obj).rebuild_pipeline(self.render_pass.clone());
		}
	}

	pub fn set_mouse_lock(&mut self, lock: bool) {
		let _ = self.surface.window().set_cursor_grab(lock);
		self.surface.window().set_cursor_visible(!lock);
	}

	pub fn add_light(&mut self, light: Light) -> Result<(), UpdateError<()>> {
		if self.light_count >= 4 {
			Err(UpdateError::BufferFull)
		} else {
			self.lights[self.light_count as usize] = light;
			self.light_count += 1;
			Ok(())
		}
	}

	pub fn rebuild_swapchain(&mut self) {
		// Get the new dimensions of the window.
		self.settings.size = self.surface.window().inner_size().into();
		let (new_swapchain, new_images) = {
			match self
				.swapchain
				.recreate_with_dimensions([self.settings.size.0, self.settings.size.1])
			{
				Ok(r) => r,
				// This error tends to happen when the user is manually resizing the window.
				// Simply restarting the loop is the easiest way to fix this issue.
				//TODO: This might need to change; I think it's supposed to terminate the entire draw pass
				Err(SwapchainCreationError::UnsupportedDimensions) => return,
				Err(e) => panic!("Failed to recreate swapchain: {:?}", e),
			}
		};
		self.swapchain = new_swapchain;
		let intermediary = AttachmentImage::transient_multisampled(
			self.device.clone(),
			[self.settings.size.0, self.settings.size.1],
			self.settings.antialiasing,
			self.format,
		)
		.unwrap();

		// Because framebuffers contains an Arc on the old swapchain, we need to
		// recreate framebuffers as well.
		self.framebuffers = window_size_dependent_setup(
			&new_images,
			intermediary.clone(),
			self.render_pass.clone(),
			self.device.clone(),
			self.settings.antialiasing,
			&mut self.dynamic_state,
		);
		self.recreate_swapchain = false;
	}

	pub fn supports_msaa_level(&self, msaa_level: u32) -> bool {
		let supported_levels = self.limits.supported_msaa_levels;
		is_power_of_two(msaa_level) && (supported_levels & msaa_level) > 0
	}

	pub fn set_msaa_level(&mut self, msaa_level: u32) -> Result<(), UpdateError<u32>> {
		if self.supports_msaa_level(msaa_level) {
			self.settings.antialiasing = msaa_level;
			self.build_render_pass();
			self.trigger_recreate_swapchain();
			Ok(())
		} else {
			println!(
				"Tried to update msaa level to {} from {}; max supported is {}",
				msaa_level, self.settings.antialiasing, self.limits.max_msaa_level
			);
			Err(UpdateError::UnsupportedValue(msaa_level))
		}
	}

	#[allow(dead_code)]
	pub fn get_msaa_level(&self) -> u32 {
		self.settings.antialiasing
	}

	#[allow(dead_code)]
	pub fn get_default_alpha_mode(&self) -> CompositeAlpha {
		let supported_alpha = self.limits.capabilities.supported_composite_alpha;
		get_supported_alpha(supported_alpha)
	}

	#[allow(dead_code)]
	pub fn get_alpha_mode(&self) -> CompositeAlpha {
		self.alpha_mode
	}

	#[allow(dead_code)]
	pub fn set_alpha_mode(
		&mut self,
		alpha_mode: CompositeAlpha,
	) -> Result<(), UpdateError<CompositeAlpha>> {
		let supported_alpha = self.limits.capabilities.supported_composite_alpha;
		if supported_alpha.supports(alpha_mode) {
			self.alpha_mode = alpha_mode;
			self.build_render_pass();
			self.trigger_recreate_swapchain();
			Result::Ok(())
		} else {
			Result::Err(UpdateError::UnsupportedValue(alpha_mode))
		}
	}

	pub fn add_camera(&mut self, camera: Box<dyn RenderServerCamera>) -> usize {
		self.cameras.push(camera);
		self.cameras.len() - 1
	}

	pub fn set_camera(&mut self, camera: usize) {
		self.current_camera = camera;
	}

	// pub fn add_objects(&mut self, objects: Vec<Box<dyn RenderServerMeshInstance>>) {
	// 	for object in objects.into_iter() {
	// 		self.add_object(object);
	// 	}
	// }

	pub fn trigger_recreate_swapchain(&mut self) {
		self.recreate_swapchain = true;
	}

	pub fn update_projection_matrix(&mut self) {
		// when the window resizes, we need to rebuild the projection matrix to match the new aspect ratio
		let new_aspect_ratio = self.settings.size.0 as f32 / self.settings.size.1 as f32;
		if self.aspect_ratio != new_aspect_ratio {
			self.aspect_ratio = new_aspect_ratio;
			self.projection_matrix =
				common::get_projection_matrix(Deg(90.0).into(), self.aspect_ratio, 0.01, 100.0);
		}
	}

	pub fn set_window_size(&mut self, size: (u32, u32)) {
		self
			.surface
			.window()
			.set_inner_size(Size::Physical(PhysicalSize::new(size.0, size.1)));
	}

	pub fn get_clear_data(&self) -> Vec<ClearValue> {
		if self.settings.antialiasing > 1 {
			vec![
				[0.0, 0.0, 0.0, 1.0].into(),
				ClearValue::None,
				ClearValue::Depth(1.0),
			]
		} else {
			vec![[0.0, 0.0, 0.0, 1.0].into(), ClearValue::Depth(1.0)]
		}
	}

	pub fn get_view_matrix(&self) -> Matrix4 {
		let current_camera = self.cameras.get(self.current_camera).unwrap();
		Matrix4::from(current_camera.get_rotation())
			* Matrix4::from_translation(-current_camera.get_position())
	}

	pub fn poll_messages(&mut self) {
		let receiver = self.server_interop.receiver.clone();
		for message in receiver.try_iter() {
			use RenderServerMessage::*;
			match message {
				ChangeWindowSize(s) => {
					self.set_window_size(s);
				}
				InvalidateSwapchain => {
					self.trigger_recreate_swapchain();
				}
				SetMouseGrab(grab) => {
					self.set_mouse_lock(grab);
				}
				SetDebugMode(mode) => {
					self.debug_mode = mode;
				}
				UpdateMousePosition(pos) => {
					self.update_mouse_position(pos);
				}
				SetMousePosition(pos) => {
					self.set_mouse_position(pos);
				}
				AddObject(obj) => {
					self.add_object(obj);
				}
				AddCamera(cam) => {
					let index = self.add_camera(cam);
					self.set_camera(index);
				}
			}
		}
	}

	pub fn add_object(&mut self, mut object: Box<dyn Renderer>) {
		object.initialize(self.device.clone(), self.render_pass.clone());
		self.objects.push(object);
	}

	/// This updates the internal mouse position stored value. This should ONLY be called in response to WindowEvents!!!
	pub fn update_mouse_position(&mut self, m_pos: PhysicalPosition<f64>) {
		self.mouse_position = m_pos;
	}
	/// This sets the mouse position on screen relative to the window.
	pub fn set_mouse_position(&mut self, new_pos: PhysicalPosition<f64>) {
		self
			.surface
			.window()
			.set_cursor_position(new_pos)
			.expect("Failed to set mouse pos");
	}

	#[allow(dead_code)]
	pub fn get_mouse_position(&self) -> PhysicalPosition<f64> {
		self.mouse_position
	}

	#[allow(dead_code)]
	pub fn inter_frame_step(&mut self) {
			//.clear_mouse_motion(); 
			for cam in self.cameras.iter_mut() {
				cam.poll_messages();
			}
			for obj in self.objects.iter_mut() {
				obj.poll_messages();
			}
	}

	/// Draws a single step to the screen.
	pub fn draw_frame(&mut self, delta: f64) {
		// Cleaning up the garbage
		self
			.previous_frame_end
			.as_mut()
			.expect("Failed to get previous frame end")
			.cleanup_finished();

		// Whenever the window resizes we need to recreate everything dependent on the window size
		if self.recreate_swapchain {
			self.rebuild_swapchain();
			self.recreate_swapchain = false;
		}
		self.update_projection_matrix();

		// Gets the next available image or triggers a swapchain rebuild as necessary
		let (image_num, suboptimal, acquire_future) =
			match swapchain::acquire_next_image(self.swapchain.clone(), None) {
				Ok(r) => r,
				Err(AcquireError::OutOfDate) => {
					self.recreate_swapchain = true;
					return;
				}
				Err(e) => panic!("Failed to acquire next image: {:?}", e),
			};
		// acquire_next_image can be successful, but suboptimal. This means that the swapchain image
		// will still work, but it may not display correctly. With some drivers this can be when
		// the window resizes, but it may not cause the swapchain to become out of date.
		if suboptimal {
			self.recreate_swapchain = true;
		}

		let clear_data = self.get_clear_data();

		//TODO: Reuse command buffers
		let mut builder = AutoCommandBufferBuilder::primary_one_time_submit(
			self.device.clone(),
			self.queue.family(),
		)
		.expect("Failed to build command buffer builder");
		builder
			.begin_render_pass(self.framebuffers[image_num].clone(), false, clear_data)
			.expect("Failed to begin render pass");

		let view_matrix = self.get_view_matrix();
		// render all models
		for obj in self.objects.iter_mut() {
			(*obj).draw(
				&mut builder,
				self.dynamic_state.clone(),
				&self.projection_matrix,
				&view_matrix,
				delta,
				self.debug_mode,
			);
		}
		builder
			.end_render_pass()
			.expect("Failed to end render pass");
		let command_buffer = builder.build().expect("Failed to build command buffer");
		self.update_future(acquire_future, command_buffer, image_num);
	}

	fn update_future(
		&mut self,
		acquire_future: SwapchainAcquireFuture<Window>,
		command_buffer: AutoCommandBuffer<StandardCommandPoolAlloc>,
		image_num: usize,
	) {
		let future = self
			.previous_frame_end
			.take()
			.expect("Failed to take future")
			.join(acquire_future)
			.then_execute(self.queue.clone(), command_buffer)
			.expect("Failed to add future execution")
			.then_swapchain_present(self.queue.clone(), self.swapchain.clone(), image_num)
			.then_signal_fence_and_flush();

		match future {
			Ok(future) => {
				self.previous_frame_end = Some(future.boxed());
			}
			Err(FlushError::OutOfDate) => {
				self.recreate_swapchain = true;
				self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
			}
			Err(e) => {
				println!("Failed to flush future: {:?}", e);
				self.previous_frame_end = Some(sync::now(self.device.clone()).boxed());
			}
		}
	}
}
