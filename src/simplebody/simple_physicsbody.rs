use crate::{
	common::{math_types::*, TwoWay},
	asset_loading::mesh_instance::RenderObjectMessage,
	physics::{
		physics_object::{PhysicsObject, PhysicsObjectMessage},
		physics_server::PhysicsServerMessage,
	},
};
use cgmath::Euler;
use input_map::InputMap;
use std::sync::RwLockReadGuard;

pub struct SimplePhysicsBody {
	position: Vector3,
	rotation: Quaternion,
	messager: TwoWay<PhysicsObjectMessage, RenderObjectMessage>,
}

impl SimplePhysicsBody {
	pub fn new(
		_path: String,
		messager: TwoWay<PhysicsObjectMessage, RenderObjectMessage>,
		position: Vector3,
		rotation: Quaternion,
	) -> SimplePhysicsBody {
		SimplePhysicsBody {
			position,
			rotation,
			messager,
		}
	}
}

impl PhysicsObject for SimplePhysicsBody {
	fn get_position(&self) -> Vector3 {
		self.position
	}
	fn get_rotation(&self) -> Quaternion {
		self.rotation
	}
	fn step_physics(
		&mut self,
		index: usize,
		delta: f32,
		_input_map: &RwLockReadGuard<InputMap>,
	) -> PhysicsServerMessage {
		self.rotation = self.rotation
			* Quaternion::from(Euler::new(
				cgmath::Rad(0.0),
				cgmath::Rad(delta),
				cgmath::Rad(0.0),
			));
		if let Err(_) = self
			.messager
			.sender
			.send(RenderObjectMessage::UpdateRotation(self.rotation))
		{
			// if the other end has disconnected it means the body no longer exists on the render server; to sync we need to remove the object
			// from the physics server as well
			PhysicsServerMessage::RemoveObject(index)
		} else {
			PhysicsServerMessage::None
		}
	}
}
