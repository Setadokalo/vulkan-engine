use crate::common::math_types::*;

pub trait RenderServerCamera {
	fn get_position(&self) -> Vector3;
	fn get_rotation(&self) -> Quaternion;
	fn poll_messages(&mut self);
}
