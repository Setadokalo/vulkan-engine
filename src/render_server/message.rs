use winit::dpi::PhysicalPosition;

use crate::asset_loading::mesh_instance::Renderer;

use super::render_camera::RenderServerCamera;

pub enum RenderServerMessage {
	#[allow(dead_code)]
	ChangeWindowSize((u32, u32)),
	InvalidateSwapchain,
	SetMouseGrab(bool),
	SetDebugMode(u32),
	UpdateMousePosition(PhysicalPosition<f64>),
	#[allow(dead_code)]
	SetMousePosition(PhysicalPosition<f64>),

	AddObject(Box<dyn Renderer>),
	AddCamera(Box<dyn RenderServerCamera>),
}
pub enum RenderObjectMessage {

}