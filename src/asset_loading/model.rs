use super::{mesh::{GpuMesh, abstract_mesh::AbstractMesh}, LoadError, offloaded_texture::OffloadedTexture};
use std::{time::Instant, sync::{mpsc, Arc}, thread};
use vulkano::{format::Format, image::ImmutableImage, device::Device, pipeline::raster::CullMode, buffer::{BufferUsage, CpuAccessibleBuffer}};
use crate::common::{Vertex, math_types::Vector4};

pub mod collada;
pub mod obj;


pub fn load_model(path: &str) -> std::result::Result<Model, LoadError> {
	if path.ends_with(".obj") {
		obj::load_model(path)
	} else if path.ends_with(".dae") {
		collada::load_model(path)
	} else {
		Err(LoadError::MalformedFile)
	}
}

pub type Color = Vector4;

enum ModelParamId {
	Mesh      = 1,
	Albedo    = 2,
	Normal    = 4,
	Tangent   = 8,
	Bitangent = 16,
	Roughness = 32,
	Metallic  = 64,
}
#[derive(Debug, Default)]
pub struct LoadedState {
	loaded_state: i32,
}
impl LoadedState {
	#[inline]
	fn new() -> Self {
		Self {
			loaded_state: 0
		}
	}
	#[inline]
	fn is_loaded(&self, id: ModelParamId) -> bool {
		self.loaded_state & (id as i32) > 0
	}
	#[inline]
	fn mark_loaded(&mut self, id: ModelParamId) {
		self.loaded_state = self.loaded_state | (id as i32);
	}
}
impl Into<i32> for LoadedState {
    fn into(self) -> i32 {
        self.loaded_state
    }
}
impl From<i32> for LoadedState {
    fn from(i: i32) -> Self {
        Self {
			  loaded_state: i
		  }
    }
}
#[test]
fn loaded_state() {
	use std::any::Any;
	let x: i32 = LoadedState::new().into();
}
#[derive(Debug)]
pub struct Model {
	loaded_state: LoadedState,
	// PBR info
	albedo_texture: Option<OffloadedTexture>,
	albedo: Color,
	normal_texture: Option<OffloadedTexture>,
	roughness_texture: Option<OffloadedTexture>,
	roughness: f32,
	metallic_texture: Option<OffloadedTexture>,
	metallic: f32,
	// Render info
	cull_face: CullMode,
	// Model info
	mesh_data: Option<GpuMesh>,
	start: Instant,

	receiver: Option<mpsc::Receiver<(Vec<Vertex>, Vec<u32>)>>,
}
impl Default for Model {
    fn default() -> Self {
		 Self {
			loaded_state: Default::default(),
			albedo_texture: Default::default(),
			albedo: Color::new(1.0, 1.0, 1.0, 1.0),
			normal_texture: Default::default(),
			roughness_texture: Default::default(),
			roughness: Default::default(),
			metallic_texture: Default::default(),
			metallic: Default::default(),
		   cull_face: CullMode::Back,
		   mesh_data: Default::default(),
		   start: Instant::now(),
		   receiver: Default::default(),
		 }
    }
}
impl Model {
	pub fn new() -> Self {
		Self {
			albedo: Color::new(1.0, 0.0, 0.0, 1.0),
			roughness: 1.0,
			cull_face: CullMode::Back,
			start: Instant::now(),
			..Default::default()
		}
	}

	pub fn load_from_path(path: String) -> Self {

		let (sender, _receiver) = mpsc::channel();

		let temp_path = path.clone();
		thread::spawn(move || {

			let model = load_model(&temp_path[..])
				.expect("Failed to load model");

			sender
				.send(model)
				.expect("Failed to send texture data to main thread!");
		});
		todo!();
		// return Model {
		// 	albedo: Some(0),
		// 	albedo_texture: None,
		// 	normal_texture: None,
		// 	roughness: Some(0),
		// 	roughness_texture: None,
		// 	metallic: Some(0),
		// 	metallic_texture: None,
		// 	cull_face: CullMode::Back,
		// 	mesh_data: None,
		// 	start: Instant::now(),
		// 	receiver: Some(receiver),
		// };
	}
	pub fn get_albedo(&mut self) -> Arc<ImmutableImage<Format>> {
		self.albedo_texture.as_mut().unwrap().get_texture()
	}
	pub fn get_normal(&mut self) -> Arc<ImmutableImage<Format>> {
		self.normal_texture.as_mut().unwrap().get_texture()
	}
}

impl AbstractMesh for Model {
	fn get_data(
		&mut self,
		device: Arc<Device>,
	) -> Option<&GpuMesh> {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if self.mesh_data.is_none() {
			match self.receiver.as_ref().expect("Failed to get receiver").try_recv() {
				Ok((vertex_data, index_data)) => {
					self.mesh_data = Some(GpuMesh::new(
						CpuAccessibleBuffer::from_iter(
								device.clone(),
								BufferUsage::vertex_buffer(),
								false,
								vertex_data.into_iter(),
							)
							.expect("Failed to build vertex buffer"),
						vulkano::buffer::cpu_access::CpuAccessibleBuffer::from_iter(
								device.clone(),
								BufferUsage::index_buffer(),
								false,
								index_data.into_iter(),
							)
							.expect("Failed to build index buffer"),
					));
					println!(
						"[Offloaded Model] Finished loading model; took {:.5} seconds",
						self.start.elapsed().as_secs_f64()
					);
					let md = self.mesh_data.as_ref().expect("Mesh data was empty!");
					return Some(md);
				}
				Err(_) => {
					return None;
				}
			}
		} else {
			let md = self.mesh_data.as_ref().expect("Mesh data was empty!");
			return Some(md);
		}
	}
}
