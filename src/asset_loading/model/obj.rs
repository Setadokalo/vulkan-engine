use crate::{
	common::{
		arr_to_vec,
		math_types::*,
		vec_math::{ThreeDMath, VectorMath},
		Vertex,
	},
	asset_loading::LoadError, asset_loading::model::Model,
};
use obj::{IndexTuple, ObjData};

//TODO: this tangent/bitangent calculation is completely broken and makes lighting not work right at all.
fn checked_add_tri(
	data: &mut Vec<Vertex>,
	index_array: &mut Vec<u32>,
	obj_data: &ObjData,
	tri_vertices: [&IndexTuple; 3],
) {
	// we're loading the vertex data from the model and generating tangent/bitangent data here
	// vertex position shortcuts
	let v0: Vector3 = arr_to_vec(obj_data.position[tri_vertices[0].0]);
	let v1: Vector3 = arr_to_vec(obj_data.position[tri_vertices[1].0]);
	let v2: Vector3 = arr_to_vec(obj_data.position[tri_vertices[2].0]);
	// uv shortcuts

	let mut uv0 = Vector2::from(match tri_vertices[0].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0],
	});
	let mut uv1 = Vector2::from(match tri_vertices[1].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0],
	});
	let mut uv2 = Vector2::from(match tri_vertices[2].1 {
		Some(index) => obj_data.texture[index],
		None => [0.0, 0.0],
	});

	uv0.y *= -1.0;
	uv1.y *= -1.0;
	uv2.y *= -1.0;

	let delta_pos_1: Vector3 = v1 - v0;
	let delta_pos_2: Vector3 = v2 - v0;

	let delta_uv_1: Vector2 = uv1 - uv0;
	let delta_uv_2: Vector2 = uv2 - uv0;
	let r = 1.0_f32 / (delta_uv_1[0] * delta_uv_2[1] - delta_uv_1[1] * delta_uv_2[0]);
	let tan: Vector3 = (delta_pos_1 * delta_uv_2[1]) - (delta_pos_2 * delta_uv_1[1]) * r;

	let bitan: Vector3 =
		((&delta_pos_2 * delta_uv_1[0] - &delta_pos_1 * delta_uv_2[0]) * r).normalized();

	let mut normal = arr_to_vec(match tri_vertices[0].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0],
	});
	let mut first_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		first_tan = first_tan * -1.0;
	}
	let first_vert = Vertex::new(
		v0.into(),
		Some(uv0.into()),
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		Some(normal.into()),
		Some(first_tan.into()),
		Some(bitan.into()),
	);
	normal = arr_to_vec(match tri_vertices[1].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0],
	});
	let mut second_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		second_tan = second_tan * -1.0;
	}
	let second_vert = Vertex::new(
		v1.into(),
		Some(uv1.into()),
		Some(normal.into()),
		Some(second_tan.into()),
		Some(bitan.into()),
	);
	normal = arr_to_vec(match tri_vertices[2].2 {
		Some(index) => obj_data.normal[index],
		None => [0.0, 1.0, 0.0],
	});
	let mut third_tan: Vector3 = (tan - normal * normal.dot(tan)).normalized();
	if normal.cross(tan).dot(bitan) < 0.0 {
		third_tan = third_tan * -1.0;
	}
	let third_vert = Vertex::new(
		v2.into(),
		Some(uv2.into()),
		// not having a normal value is probably undefined behaviour and should be handled as such... we'll see
		Some(normal.into()),
		Some(third_tan.into()),
		Some(bitan.into()),
	);
	check_and_add_vert(data, index_array, first_vert);
	check_and_add_vert(data, index_array, second_vert);
	check_and_add_vert(data, index_array, third_vert);
}

fn check_and_add_vert(
	data: &mut Vec<Vertex>,
	index_array: &mut Vec<u32>,
	mut current_vert: Vertex,
) {
	// I doubt this is the most efficient way to do this, but from everything I tried this was the only way I got to work. TODO: try making this better
	// Using rposition instead of position because most repeat vertices will most likely be recently added
	// For reference - loading the Utah Teapot takes ~1.87 seconds using `position` and ~0.45 seconds using `rposition`
	let vert_index = match data.iter().rposition(|&v| v == current_vert) {
		Some(v) => v as i64,
		None => -1,
	};
	if vert_index != -1 {
		// combine the tangent and bitangent values
		let target_vert = data
			.get(vert_index as usize)
			.expect("failed to read vertex");
		current_vert = current_vert.join(*target_vert);
		data[vert_index as usize] = current_vert;
		index_array.push(vert_index as u32); // using -1 as the default index because otherwise there'd be confusion about what is and isn't a valid point -
	                                  // is Integer.MAX_VALUE the index of the vertex, or the value indicating no data?
	} else {
		data.push(current_vert);
		index_array.push((data.len() - 1) as u32);
	}
}

pub fn load_model(
	path: &str
) -> Result<Model, LoadError> {

	let object = obj::Obj::load(path).expect("Object failed to load");

	let mut vertices = Vec::new();
	let mut indices = Vec::new();

	for obj_object in object.data.objects.iter() {
		for obj_group in obj_object.groups.iter() {
			// assuming each polygon is a quad since they're the most common and checking each polygon individually would be way less efficient
			// this just serves to reduce reallocations anyway, not set a fixed size or anything, so it's fine*.
			vertices.reserve(obj_group.polys.len() * 4);

			for poly in obj_group.polys.iter() {
				// Converts N-GONS to triangles using simple triangle fan triangulation
				// We're assuming all polygons are convex because concave triangulation looks like a nightmare and I'm not about that life
				// sanity check to ensure all polygons have at least 3 points
				if poly.0.len() < 3 {
					return Err(LoadError::MalformedFile);
				}
				// if any of these `unwrap`s ever cause a panic I'm nuking this project and moving to a farm in the country
				let tri_start_vert = poly.0.get(0).unwrap();
				for i in 2..poly.0.len() {
					let tri_mid_vert = poly.0.get(i - 1).unwrap();
					let tri_last_vert = poly.0.get(i).unwrap();
					checked_add_tri(
						&mut vertices,
						&mut indices,
						&object.data,
						[tri_start_vert, tri_mid_vert, tri_last_vert],
					);
				}
			}
		}
	}
	// removes extra space at the end of the buffer if it exists; no sense in wasting memory
	vertices.shrink_to_fit();
	todo!();
	// let model = Model::new();
	// Ok(model)
}
