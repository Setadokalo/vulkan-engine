use crate::common::{math_types::Vector4, Vertex};
use std::sync::Arc;
use vulkano::buffer::CpuAccessibleBuffer;

pub mod abstract_mesh {
	
	use std::sync::Arc;
	use vulkano::device::Device;
	use super::GpuMesh;

	pub trait AbstractMesh {
		fn get_data(
			&mut self,
			device: Arc<Device>,
		) -> Option<&GpuMesh>;
	}
}

#[derive(Debug)]
pub struct GpuMesh {
	vertex_buffer: Arc<CpuAccessibleBuffer<[Vertex]>>,
	index_buffer: Arc<CpuAccessibleBuffer<[u32]>>,
}
impl GpuMesh {
	pub fn get_vertex_buffer(&self) -> Arc<CpuAccessibleBuffer<[Vertex]>> {
		self.vertex_buffer.clone()
	}
	pub fn get_index_buffer(&self) -> Arc<CpuAccessibleBuffer<[u32]>> {
		self.index_buffer.clone()
	}
	pub fn new(vertex_buffer: Arc<CpuAccessibleBuffer<[Vertex]>>, index_buffer: Arc<CpuAccessibleBuffer<[u32]>>) -> Self {
		GpuMesh {
			vertex_buffer,
			index_buffer,
		}
	}
}
