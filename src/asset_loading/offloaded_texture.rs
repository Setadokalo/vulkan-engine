use std::{convert::TryInto, fmt::Display, sync::Arc};

use crate::asset_loading;
use same_file::Handle;
use vulkano::{
	device::Queue, format::Format, image::Dimensions, image::ImageCreationError,
	image::ImmutableImage,
};
#[derive(Debug)]
enum PossiblyLoaded<T> {
	Loaded(T),
	NotLoaded {
		handle: Handle,
		path: String,
		prefer_srgb: bool,
	},
}

#[derive(Debug)]
pub struct RawTextureData {
	pub bytes: Vec<u8>,
	pub size: (u32, u32),
	pub format: Format,
	queue: Option<Arc<Queue>>,
}

impl RawTextureData {
	pub fn set_queue(&mut self, queue: Arc<Queue>) {
		self.queue = Some(queue);
	}
	pub fn new(bytes: Vec<u8>, size: (u32, u32), format: Format, queue: Option<Arc<Queue>>) -> Self {
		Self {
			bytes,
			size,
			format,
			queue,
		}
	}
}

#[derive(Debug)]
/// Represents a reason for a failed conversion to vulkano resources.
pub enum ConversionError {
	MissingQueue,
	ImageCreationError(Box<dyn std::error::Error>),
}
impl ConversionError {
	fn enum_to_display(&self) -> String {
		match self {
			ConversionError::MissingQueue => "Structure did not have a queue for converting".into(),
			ConversionError::ImageCreationError(err) => {
				format!("Vulkano Image Creation Error: {}", err)
			}
		}
	}
}

impl Display for ConversionError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(&*format!(
			"Failed to convert image: {}",
			self.enum_to_display()
		))
	}
}

impl std::error::Error for ConversionError {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		None
	}
}
impl From<ImageCreationError> for ConversionError {
	fn from(other: ImageCreationError) -> Self {
		ConversionError::ImageCreationError(Box::new(other))
	}
}

impl TryInto<Arc<ImmutableImage<Format>>> for RawTextureData {
	type Error = ConversionError;

	fn try_into(self) -> Result<Arc<ImmutableImage<Format>>, Self::Error> {
		Ok(ImmutableImage::from_iter(
			self.bytes.iter().cloned(),
			Dimensions::Dim2d {
				width: self.size.0,
				height: self.size.1,
			},
			self.format,
			self
				.queue
				.as_ref()
				.ok_or(Self::Error::MissingQueue)?
				.clone(),
		)?
		.0)
	}
}

/// This struct stores and provides a texture that is loaded from the disk on a second thread.
/// For textures it is better to provide an empty texture than return a conditionally valid resource imo, so we return `empty_texture`
/// when the code asks for the texture before it's ready
#[derive(Debug)]
pub struct OffloadedTexture {
	empty_texture: Arc<ImmutableImage<Format>>,
	texture: PossiblyLoaded<Arc<ImmutableImage<Format>>>,
	queue: Arc<Queue>,
}

impl OffloadedTexture {
	/// Constructs an OffloadedTexture that loads the image file at `path` and returns `empty_texture` if the image hasn't loaded yet
	#[allow(unused_variables)]
	pub fn new(
		path: String,
		queue: Arc<Queue>,
		empty_texture: Arc<ImmutableImage<Format>>,
		srgb: bool,
	) -> OffloadedTexture {
		let handle = Handle::from_path(path.clone()).expect("failed to get handle for texture file");
		todo!();
		// let texture = file_loading::get_loading_handle()
		// 	.lock()
		// 	.expect("failed to get safe lock")
		// 	.load_raw_image(&handle, path.clone(), srgb);
		// OffloadedTexture {
		// 	empty_texture,
		// 	texture: PossiblyLoaded::NotLoaded {
		// 		handle,
		// 		path,
		// 		prefer_srgb: srgb,
		// 	},
		// 	queue,
		// }
	}

	/// Returns either the loaded texture or the empty texture if the loaded texture is not ready.
	pub fn get_texture<'b>(&'b mut self) -> Arc<ImmutableImage<Format>> {
		// this is done in this if else structure instead of through a match because using a match causes reference scope issues.
		if let PossiblyLoaded::NotLoaded {
			handle,
			path,
			prefer_srgb,
		} = &self.texture
		{
			todo!();
			// if let Some(data) = file_loading::get_loading_handle()
			// 	.lock()
			// 	.expect("Failed to get safe lock")
			// 	.load_image(handle, path.clone(), *prefer_srgb, self.queue.clone())
			// {
			// 	self.texture =
			// 		PossiblyLoaded::Loaded(data);
			// }
		}
		if let PossiblyLoaded::Loaded(texture) = &self.texture {
			texture.clone()
		} else {
			self.empty_texture.clone()
		}
	}
}
#[test]
fn texture_size() {
	println!("OffloadedTexture size: {}", std::mem::size_of::<OffloadedTexture>())
}