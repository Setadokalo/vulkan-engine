use super::{LoadError, FILENAME_R};
use crate::asset_loading::offloaded_texture::RawTextureData;
use image::{ColorType, ImageDecoder};
use regex::Regex;
use std::{fs::File, io::Read};

pub fn load_texture(path: String, prefer_srgb: bool) -> RawTextureData {
	let start_time = std::time::Instant::now();
	let mut dfile = File::open(path.clone()).expect("Failed to open file");
	// glium::Texture2d::new(facade, data).expect("Failed to generate texture")
	let (mut data, color_type, dimensions) = if path.ends_with(".png") {
		load_texture_png(&mut dfile)
	} else if path.ends_with(".jpg") || path.ends_with(".jpeg") {
		load_texture_jpg(&mut dfile)
	} else if path.ends_with(".ff") || path.ends_with(".farbfeld") {
		//.farbfeld is technically invalid but I bet some people use it
		load_texture_ff(&mut dfile)
	} else {
		panic!("Invalid texture type!");
	}
	.expect("File load error");
	// It doesn't matter if it's BGR or RGB, if the bpp is 24 we need to add an alpha channel
	if color_type.bits_per_pixel() == 24 {
		data = add_alpha_channel(data);
	} else if color_type.bits_per_pixel() == 48 {
		data = add_alpha_channel_16(data);
	}
	// sigh... Rust, can you trust me just a LITTLE bit? static mut isn't THAT evil
	let r = unsafe {
		match &FILENAME_R {
			Some(regex) => regex,
			None => {
				FILENAME_R = Some(Regex::new(r"(.*/)*(.*?\..*?$)").expect("Failed to build regex"));
				FILENAME_R.as_ref().unwrap()
			}
		}
	};
	let filename_opt = r.captures(&*path);
	if let Some(filename_cap) = filename_opt {
		if let Some(filename) = filename_cap.get(2) {
			println!(
				"[Image Decoder] Returning image \"{}\"; took {:.4} seconds",
				filename.as_str(),
				start_time.elapsed().as_secs_f64()
			);
		} else {
			println!(
				"[Image Decoder] Returning constructed image; took {:.4} seconds",
				start_time.elapsed().as_secs_f64()
			);
		}
	} else {
		println!(
			"[Image Decoder] Returning constructed image; took {:.4} seconds",
			start_time.elapsed().as_secs_f64()
		);
	}

	RawTextureData::new(
		data,
		dimensions,
		super::convert_ctype_to_format(color_type, prefer_srgb),
		None,
	)
}

pub fn load_texture_png(dfile: &mut File) -> Result<(Vec<u8>, ColorType, (u32, u32)), LoadError> {
	let mut raw_data_buf = Vec::new();
	dfile.read_to_end(&mut raw_data_buf)?;
	let img = image::png::PngDecoder::new(&raw_data_buf[..]).expect("Failed to load image file");
	let color_type = img.color_type();
	let total_bytes = img.total_bytes() as usize;
	let mut data: Vec<u8> = vec![0u8; total_bytes];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..])
		.expect("Failed to convert image to raw data");
	Ok((data, color_type, dimensions))
}

// This function zippers an alpha channel onto 8-bpc textures, since GPUs almost never support 24-bit color.
fn add_alpha_channel(opaque_data: Vec<u8>) -> Vec<u8> {
	// this is ugly and probably very bad code but I'm tired so fuck it
	let total_bytes = opaque_data.len();
	let mut data: Vec<u8> = vec![255u8; total_bytes / 3 * 4];
	let mut bytes_offset = 0_usize;
	for i in 0..total_bytes {
		data[i + bytes_offset] = opaque_data[i];
		if i % 3 == 2 {
			bytes_offset = bytes_offset + 1;
		}
	}
	data
}

// WARNING: THIS IS COMPLETELY UNTESTED. I don't have any 16 bpc textures to test with and I really can't be fucked to go find one.
// Use at your own risk.
// This function zippers an alpha channel onto 16-bpc textures, since GPUs almost never support 48-bit color.
fn add_alpha_channel_16(opaque_data: Vec<u8>) -> Vec<u8> {
	// this is ugly and probably very bad code but I'm tired so fuck it
	let total_bytes = opaque_data.len();
	let mut data: Vec<u8> = vec![255u8; total_bytes / 3 * 4];
	let mut bytes_offset = 0_usize;
	for i in 0..total_bytes {
		data[i + bytes_offset] = opaque_data[i];
		if i % 6 == 5 {
			bytes_offset = bytes_offset + 2;
		}
	}
	data
}

pub fn load_texture_jpg(dfile: &mut File) -> Result<(Vec<u8>, ColorType, (u32, u32)), LoadError> {
	let mut raw_data_buf = Vec::new();
	dfile.read_to_end(&mut raw_data_buf)?;
	let img = image::jpeg::JpegDecoder::new(&raw_data_buf[..]).expect("Failed to load image file");
	let color_type = img.color_type();
	let total_bytes = img.total_bytes() as usize;
	// since this is a jpg, the data loaded in will always be RGB; the GPU will only accept RGBA so we need to zipper in an alpha channel
	let mut data: Vec<u8> = vec![0u8; total_bytes];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..])
		.expect("Failed to convert image to raw data");
	Ok((data, color_type, dimensions))
}

pub fn load_texture_ff(dfile: &mut File) -> Result<(Vec<u8>, ColorType, (u32, u32)), LoadError> {
	let mut raw_data_buf = Vec::new();
	dfile.read_to_end(&mut raw_data_buf)?;
	let img =
		image::farbfeld::FarbfeldDecoder::new(&raw_data_buf[..]).expect("Failed to load image file");
	let color_type = img.color_type();
	let mut data: Vec<u8> = vec![0u8; img.total_bytes() as usize];
	let dimensions = img.dimensions();
	img.read_image(&mut data[..])
		.expect("Failed to convert image to raw data");
	Ok((data, color_type, dimensions))
}
