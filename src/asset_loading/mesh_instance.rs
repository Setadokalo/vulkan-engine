use super::{mesh::GpuMesh, Pipeline};
use crate::common::{extract_topleft_matrix, math_types::*, Vertex};
use shaders::{fs, vs};
use std::sync::Arc;
use vulkano::{
	buffer::{BufferUsage, CpuBufferPool},
	command_buffer::{AutoCommandBufferBuilder, DynamicState},
	descriptor::{descriptor_set::FixedSizeDescriptorSetsPool, PipelineLayoutAbstract},
	device::Device,
	framebuffer::{RenderPassAbstract, Subpass},
	image::ImageViewAccess,
	pipeline::{depth_stencil::DepthStencil, GraphicsPipeline},
	sampler::Sampler,
};

/// Trait for 
pub trait PbrRenderable {
	fn get_mesh(
		&mut self,
		device: Arc<Device>,
	) -> Option<&GpuMesh>;
	fn get_model_matrix(&self) -> Matrix4;
	fn get_albedo_texture(&mut self) -> Arc<dyn ImageViewAccess + Send + Sync>;
	fn get_normal_texture(&mut self) -> Arc<dyn ImageViewAccess + Send + Sync>;
	fn get_uv_transforms(&self) -> (Vector2, Vector2);
	fn update_data(&mut self, position: Vector3, rotation: Quaternion);
	fn poll_messages(&mut self);
	fn scale_uvs(&mut self, scale: Vector2);
	fn translate_uvs(&mut self, translation: Vector2);
}
pub trait Renderer {
	/// Initializes the Renderer; required to be called before requesting the renderer to draw for the first time.
	fn initialize(&mut self,
		device: Arc<Device>,
		render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,);
	fn draw(
		&mut self,
		builder: &mut AutoCommandBufferBuilder,
		dynamic_state: DynamicState,
		projection_matrix: &Matrix4,
		view_matrix: &Matrix4,
		delta: f64,
		debug_mode: u32,
	);
	fn rebuild_pipeline(&mut self, render_pass: Arc<dyn RenderPassAbstract + Send + Sync>);
	fn poll_messages(&mut self);
}
pub struct PbrRenderer<T: PbrRenderable> {
	object: T,

	initialized: bool,

	device: Option<Arc<Device>>,
	pipeline: Option<Pipeline>,
	pool: Option<FixedSizeDescriptorSetsPool>,
	vs: Option<vs::Shader>,
	fs: Option<fs::Shader>,

	vertex_uniform_buffer: Option<CpuBufferPool<vs::ty::matrices>>,
	uv_uniform_buffer: Option<CpuBufferPool<fs::ty::frag_info>>,
}

impl<'a, T: PbrRenderable> PbrRenderer<T> {

	/// Constructs a new PbrRenderer for the given object.
	pub fn new(
		object: T,
	) -> Self {
		Self {
			object,
			initialized: false,
			device: None,
			pipeline: None,
			pool: None,
			vs: None,
			fs: None,
			vertex_uniform_buffer: None,
			uv_uniform_buffer: None,
		}
	}

	fn generate_pipeline(
		render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
		vs: &vs::Shader,
		fs: &fs::Shader,
		device: Arc<Device>,
	) -> Arc<
		GraphicsPipeline<
			vulkano::pipeline::vertex::SingleBufferDefinition<Vertex>,
			Box<dyn PipelineLayoutAbstract + Send + Sync>,
			Arc<dyn RenderPassAbstract + Send + Sync>,
		>,
	> {
		let pre_pipeline = GraphicsPipeline::start()
			// Defines what kind of vertex input is expected.
			.vertex_input_single_buffer::<Vertex>()
			// The vertex shader.
			.vertex_shader(vs.main_entry_point(), ());
		let pre_pipeline = pre_pipeline
			.front_face_counter_clockwise()
			.cull_mode_back()
			.depth_stencil(DepthStencil::simple_depth_test())
			// Defines the viewport (explanations below).
			.viewports_dynamic_scissors_irrelevant(1);
		let pre_pipeline = pre_pipeline
			// The fragment shader.
			.fragment_shader(fs.main_entry_point(), ())
			.render_pass(Subpass::from(render_pass.clone(), 0).expect("Failed to create subpass"));
		println!("Preparing to build pipeline");
		let pre_pipeline = pre_pipeline
			// Now that everything is specified, we call `build`.
			.build(device.clone())
			.expect("Failed to build pipeline");
		println!("Built pipeline!");
		Arc::new(pre_pipeline)
	}

	fn generate_descriptors(pipeline: Pipeline) -> FixedSizeDescriptorSetsPool {
		let layout = pipeline.descriptor_set_layout(0).unwrap();
		FixedSizeDescriptorSetsPool::new(layout.clone())
	}
}

impl<T: PbrRenderable> Renderer for PbrRenderer<T> {
	fn initialize(
		&mut self,
		device: Arc<Device>,
		render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
	) {
		self.device = Some(device.clone());
		self.vs = Some(vs::Shader::load(device.clone()).expect("failed to create shader module"));
		self.fs = Some(fs::Shader::load(device.clone()).expect("failed to create shader module"));

		self.vertex_uniform_buffer =
			Some(CpuBufferPool::<vs::ty::matrices>::new(device.clone(), BufferUsage::all()));
		self.uv_uniform_buffer =
			Some(CpuBufferPool::<fs::ty::frag_info>::new(device.clone(), BufferUsage::all()));

		self.pipeline = Some(Self::generate_pipeline(render_pass, &self.vs.as_ref().unwrap(), &self.fs.as_ref().unwrap(), device.clone()));
		self.initialized = true;
	}


	fn draw(
		&mut self,
		builder: &mut AutoCommandBufferBuilder,
		dynamic_state: DynamicState,
		projection_matrix: &Matrix4,
		view_matrix: &Matrix4,
		_delta: f64,
		debug_mode: u32,
	) {
		assert!(self.initialized);
		let data = self.object.get_mesh(self.device.as_ref().unwrap().clone());
		let (v_buffer, i_buffer) = match data {
			Some(mesh) => (mesh.get_vertex_buffer(), mesh.get_index_buffer()),
			None => {
				return;
			}
		};

		// Gets the next available space in each uniform buffer (I'm guessing since multiple frames can be queued
		// that buffer can have multiple entries in it) and filling it with our data for this frame.
		let (vertex_uniform_subbuf, uv_uniform_subbuf) = {
			let model_matrix = self.object.get_model_matrix();
			let vertex_data = vs::ty::matrices {
				model: model_matrix.into(),
				view: (*view_matrix).into(),
				projection: (*projection_matrix).into(),
				mvp: (projection_matrix * view_matrix * model_matrix).into(),
				mv3x3_matrix: (extract_topleft_matrix(view_matrix)
					* extract_topleft_matrix(&model_matrix))
				.into(),
			};
			let (uv_pos, uv_scale) = self.object.get_uv_transforms();
			let uv_data = fs::ty::frag_info {
				pos: uv_pos.into(),
				scale: uv_scale.into(),
				debug_mode,
			};
			// Get the next available space from each buffer and fill it with our uniforms
			(
				self.vertex_uniform_buffer.as_ref().unwrap().next(vertex_data).unwrap(),
				self.uv_uniform_buffer.as_ref().unwrap().next(uv_data).unwrap(),
			)
		};
		// From the type docs:
		// * Describes to the Vulkan implementation the layout of all descriptors within a descriptor set.
		//let layout = self.pipeline.layout().descriptor_set_layout(0).expect("Failed to get set layout");
		// The set of all uniform data to send through the pipeline
		let set = Arc::new(
			// 	PersistentDescriptorSet::start(layout.clone())
			self
				.pool
				.as_mut().unwrap()
				.next()
				.add_buffer(vertex_uniform_subbuf)
				.expect("Failed to add vertex uniform buffer")
				.add_sampled_image(
					self.object.get_albedo_texture(),
					Sampler::simple_repeat_linear_no_mipmap(self.device.as_ref().unwrap().clone()),
				)
				.expect("Failed to add albedo texture")
				.add_sampled_image(
					self.object.get_normal_texture(),
					Sampler::simple_repeat_linear_no_mipmap(self.device.as_ref().unwrap().clone()),
				)
				.expect("Failed to add albedo texture")
				.add_buffer(uv_uniform_subbuf)
				.expect("Failed to add uv uniform buffer")
				.build()
				.expect("Failed to build uniform object"),
		);

		// We're using draw_indexed here since my model loader supports indicing; if you just have a vertex buffer use `draw`
		builder
			.draw_indexed(
				self.pipeline.as_ref().unwrap().clone(),
				&dynamic_state,
				v_buffer.clone(),
				i_buffer.clone(),
				set,
				(),
			)
			.expect("Failed to draw");
	}
	fn rebuild_pipeline(&mut self, render_pass: Arc<dyn RenderPassAbstract + Send + Sync>) {
		assert!(self.initialized);
		self.pipeline = Some(Self::generate_pipeline(render_pass, &self.vs.as_ref().unwrap(), &self.fs.as_ref().unwrap(), self.device.as_ref().unwrap().clone()));
		self.pool = Some(Self::generate_descriptors(self.pipeline.as_ref().unwrap().clone()));
	}

	fn poll_messages(&mut self) {
		self.object.poll_messages();
	}
}

pub enum RenderObjectMessage {
	UpdatePosition(Vector3),
	UpdateRotation(Quaternion),
}
unsafe impl Send for RenderObjectMessage {}
unsafe impl Sync for RenderObjectMessage {}

