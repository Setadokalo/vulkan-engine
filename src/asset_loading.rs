use crate::common::{TwoWay, Vertex};
use same_file::Handle;
use std::{
	collections::HashMap,
	sync::{mpsc, Arc, Mutex, Once, Weak},
	thread, 
};
use vulkano::{descriptor::PipelineLayoutAbstract, framebuffer::RenderPassAbstract, pipeline::GraphicsPipeline, format::Format, image::ImmutableImage, device::Queue};
use ::image::ColorType;
use obj::ObjError;
use regex::Regex;
use model::Model;

pub mod mesh;
pub mod mesh_instance;
pub mod offloaded_texture;

pub type Pipeline = Arc<
	GraphicsPipeline<
		vulkano::pipeline::vertex::SingleBufferDefinition<Vertex>,
		Box<dyn PipelineLayoutAbstract + Send + Sync>,
		Arc<dyn RenderPassAbstract + Send + Sync>,
	>,
>;

pub mod model;
pub mod texture;

#[derive(Debug)]
pub enum LoadError {
	FileLoadError(std::io::Error),
	MalformedFile,
}
impl From<ObjError> for LoadError {
	fn from(obj: ObjError) -> Self {
		match obj {
			ObjError::Io(err) => LoadError::FileLoadError(err),
			_ => LoadError::MalformedFile,
		}
	}
}

impl From<std::io::Error> for LoadError {
	fn from(err: std::io::Error) -> Self {
		LoadError::FileLoadError(err)
	}
}

static mut FILENAME_R: Option<Regex> = None;

/// Converts `ColorType`s to their appropriate matching `Format`.
///
/// Note: the returned `Format` is the closest matching universally-accepted format, not necessarily the exact same bits-per-pixel.
/// Namely, any RGB/BGR format without an alpha channel will need to have one zippered on.
/// # Panics
/// This method panics if it doesn't recognize the ColorType.
fn convert_ctype_to_format(ctype: ColorType, prefer_srgb: bool) -> Format {
	match ctype {
		ColorType::L8 => Format::R8Unorm,
		ColorType::La8 => Format::R8G8Unorm,
		ColorType::Rgb8 => {
			if prefer_srgb {
				Format::R8G8B8A8Srgb
			} else {
				Format::R8G8B8A8Unorm
			}
		}
		ColorType::Rgba8 => {
			if prefer_srgb {
				Format::R8G8B8A8Srgb
			} else {
				Format::R8G8B8A8Unorm
			}
		}
		ColorType::L16 => Format::R16Sfloat,
		ColorType::La16 => Format::R16G16Sfloat,
		ColorType::Rgb16 => Format::R16G16B16A16Sfloat,
		ColorType::Rgba16 => Format::R16G16B16A16Sfloat,
		ColorType::Bgr8 => {
			if prefer_srgb {
				Format::B8G8R8A8Srgb
			} else {
				Format::B8G8R8A8Unorm
			}
		}
		ColorType::Bgra8 => {
			if prefer_srgb {
				Format::B8G8R8A8Srgb
			} else {
				Format::B8G8R8A8Unorm
			}
		}
		ColorType::__NonExhaustive(_) => panic!("Unrecognized color type!"),
	}
}

// #[derive(Hash)]
// struct UniqueFile {
// 	path: String
// }
// impl UniqueFile {
// 	pub fn new(path: String) -> UniqueFile {
// 		UniqueFile {
// 			path
// 		}
// 	}
// }
// impl PartialEq for UniqueFile {
//     fn eq(&self, other: &Self) -> bool {
//         same_file::is_same_file(self.path.as_str(), other.path.as_str()).expect("Failed to open a file")
//     }

// }
// impl Eq for UniqueFile {}




/*

Okay... time for some major work.
I want loading assets to be done through a single thread, which means I'll need a global manager for that thread.
I'd also like for file loading to not be strictly restricted to what I've prebuilt, so I'd also like to provide a
more generic interface that will allow the user to specify a custom loading function (so an Enum variant that takes an FnOnce as a parameter)
I'm not sure how much of the loading process can be done off-thread now. Previously I was just loading the data onto the CPU and then
sending that to the main thread to be sent to the gpu but I think I can do better. A couple ideas:
 * Put the load commands into a buffer before sending back to the main thread, lowering the amount of work it has to do
	- Probably wouldn't save too much load on the main thread - I'm pretty sure most of the weight is in syncing with the GPU
	- Not even sure if it's possible
 * Open a second Queue channel and load the asset on the GPU using said queue
	- Seems more likely to be possible
	- Would save a lot of time on the main thread by making it completely unassociated with sending assets to the gpu
	- Not sure you can access multiple queues on different threads?
	- Not sure you can use assets loaded by one queue on a different queue? 

*/


type GpuTexture = ImmutableImage<Format>;

#[derive(Debug, Clone)]
//TODO: Reuse the GPU objects instead of the raw CPU objects
enum WeakAsset {
	Texture(Weak<GpuTexture>),
	Model(Weak<Model>),
}

impl WeakAsset {
	fn get_strong(&self) -> Option<Asset> {
		match self {
		   WeakAsset::Texture(r) => {
				if let Some(strong_ref) = r.upgrade() {
					Some(Asset::Texture(strong_ref))
				} else {
					None
				}
			 }
		   WeakAsset::Model(r) => {
				if let Some(strong_ref) = r.upgrade() {
					Some(Asset::Model(strong_ref))
				} else {
					None
				}
			}
		}
	}
}

#[derive(Debug, Clone)]
pub enum Asset {
	Texture(Arc<GpuTexture>),
	Model(Arc<Model>),
}

impl Asset {
	fn get_weak(&self) -> WeakAsset {
		match self {
		    Asset::Texture(arc) => {
				WeakAsset::Texture(Arc::downgrade(arc))
			 }
		    Asset::Model(arc) => {
				 WeakAsset::Model(Arc::downgrade(arc))
			 }
		}
	}
}
pub enum AssetRequest<'a> {
	Texture(&'a str, bool),
	Model(&'a str),
}
impl AssetRequest<'_> {
	pub fn get_path(&self) -> &'_ str {
		match self {
			AssetRequest::Texture(s, _) => {
				s
			}
			AssetRequest::Model(s) => {
				s
			}
		}
	}
}

/// Represents a result sent back from the file loading thread.
/// These enums store a path as their identifier, because using the file handles themselves did not prove successful.
/// (File handles are used to provide a unique identifier to any file object that does not depend on it's path.)
enum LoadingResult {
	LoadedTexture { path: String, data: Arc<GpuTexture> },
	_Error { path: String, error: Box<dyn std::error::Error + Send> },
}
enum LoadingCommand {
	LoadTexture { path: String, prefer_srgb: bool },
	LoadModel(String)
}

enum Loading<W, S> {
	Loaded(W),
	// When the asset is loaded, we store a strong reference at first, then once a requesting user retrieves the object THEN we switch to storing a weak reference
	LoadedAndWaiting(S),
	Unloaded,
}

mod file_loader {
	use crate::common::TwoWay;
	use super::{LoadingResult, LoadingCommand, texture};
	use vulkano::{image::{Dimensions, ImmutableImage}, device::Queue};
	use std::sync::Arc;

	pub(super) fn run_loader_server(queue: Arc<Queue>, messenger: TwoWay<LoadingCommand, LoadingResult>) {
		
		loop {
			if let Ok(load_command) = messenger.receiver.recv() {
				match load_command {
					LoadingCommand::LoadTexture { path, prefer_srgb } => {
						let texture = texture::load_texture(path.clone(), prefer_srgb);
						let dimensions = Dimensions::Dim2d {width: texture.size.0, height: texture.size.1};
						let format = texture.format;
						let texture = ImmutableImage::from_iter(
							texture.bytes.into_iter(),
							dimensions,
							format,
							queue.clone()
						).expect("Failed to construct image").0; // Because we dropped the future here the loading server will lock until the texture is loaded
						messenger
							.send(LoadingResult::LoadedTexture{
								path: path.clone(),
								data: texture,
							})
							.expect("Failed to return texture");
					}
				    LoadingCommand::LoadModel(_) => {
						 todo!();
					 }
				}
			} else {
				// Loader server access is dead; kill the server
				break;
			}
		}
	}
}

struct OffloadedFileLoader {
	loading_thread_messaging: TwoWay<LoadingResult, LoadingCommand>,
	// We store a weak reference to allow assets to be freed if nothing is using them
	asset_cache: HashMap<Handle, Loading<WeakAsset, Asset>>,
	
}
impl OffloadedFileLoader {
	fn new(queue: Arc<Queue>) -> OffloadedFileLoader {
		// channel from file loading thread to main thread
		let (other_sen, main_recv) = mpsc::channel();
		// channel from main thread to file loading thread
		let (main_sen, other_recv) = mpsc::channel();

		thread::spawn(move || {file_loader::run_loader_server(queue, TwoWay::new(other_sen, other_recv))});
		OffloadedFileLoader {
			loading_thread_messaging: TwoWay::new(main_sen, main_recv),
			asset_cache: HashMap::new(),
		}
	}

	/// Just checks if the asset has already finished loading and if so returns it, but
	/// does not attempt to load it if it is not present
	fn check_for_asset(&mut self, handle: &Handle, path: &str) -> Result<Option<Asset>, LoadError> {
		// Use this time to empty the queue of new assets
		for result in self.loading_thread_messaging.receiver.try_iter() {
			match result {
				LoadingResult::LoadedTexture {path, data} => {
					let new_handle = Handle::from_path(path).unwrap();
					self.asset_cache.insert(new_handle, Loading::LoadedAndWaiting(Asset::Texture(data)));
				},
			   LoadingResult::_Error { path, error } => {
					eprintln!("Encountered error when loading file {}; Error is {:?}", path, error)
				}
			}
		}

		if let Some(Loading::Loaded(weak_asset)) = self.asset_cache.get(handle) {
			if let Some(asset) = weak_asset.get_strong() {
				return Ok(Some(asset));
			} else {
				self.asset_cache.remove(&handle);
			}
		} else if let Some(Loading::LoadedAndWaiting(asset)) = self.asset_cache.get(handle) {
			let ret = (*asset).clone();
			self.asset_cache.insert(Handle::from_path(path)?, Loading::Loaded(ret.get_weak()));
			return Ok(Some(ret));
		}
		Ok(None)
	}

	fn is_asset_loading(&self, handle: &Handle) -> bool {
		if let Some(_) = self.asset_cache.get(handle) {
			true
		} else {
			false
		}
	}

	/// Triggers the process of loading an asset.
	/// Should only be used internally; for getting an asset, see either `request_asset()` or `check_for_asset()`.
	fn load_asset(&mut self, a: AssetRequest) -> Result<(), LoadError> {
		let load_command = match a {
		    AssetRequest::Texture(path, prefer_srgb) => { LoadingCommand::LoadTexture {path: String::from(path), prefer_srgb}}
		    AssetRequest::Model(path) => { LoadingCommand::LoadModel(String::from(path))}
		};
		self.loading_thread_messaging.send(load_command).expect("Loading thread panicked!");
		self.asset_cache.insert(Handle::from_path(a.get_path())?, Loading::Unloaded);
		Ok(())
	}

	fn request_asset(&mut self, a: AssetRequest) -> Result<Option<Asset>, LoadError> {

		let handle = Handle::from_path(a.get_path())?;
		// If the asset has already been loaded loading it again would be wasteful
		if let Some(asset) = self.check_for_asset(&handle, a.get_path())? {
			return Ok(Some(asset));
		}
		// Make sure the asset hasn't been requested yet...
		if !self.is_asset_loading(&handle) {
			// ...then load the asset
			self.load_asset(a)?;
		}
		Ok(None)
	}
}



#[allow(dead_code)]
pub fn request_asset(a: AssetRequest) -> Result<Option<Asset>, LoadError> {
	let handle_lock = get_loading_handle();
	let mut handle = handle_lock.lock().expect("File Loader mutex holder panicked!");
	handle.request_asset(a)
}


/// Requests an asset, but will not attempt to load it if it has not already been loaded.
/// This function takes an `&str` instead of an `AssetRequest` because as it does not attempt to load the asset,
/// it does not need to know what kind of asset you expect.
#[allow(dead_code)]
pub fn request_asset_no_load(a: &str) -> Result<Option<Asset>, LoadError> {
	let loader_lock = get_loading_handle();
	let mut loader = loader_lock.lock().expect("File Loader mutex holder panicked!");
	let handle = Handle::from_path(a)?;
	loader.check_for_asset(&handle, a)
}

static LOADING_THREAD_INIT: Once = Once::new();
static mut LOADING_THREAD: Option<Arc<Mutex<OffloadedFileLoader>>> = None;

fn get_loading_handle() -> Arc<Mutex<OffloadedFileLoader>> {
	unsafe {
		LOADING_THREAD.as_ref().expect("Attempted to use File Loading Server before Render Server was initialized!!").clone()
	}
}

pub fn initialize_file_loading(queue: Arc<Queue>) {
	unsafe {
		LOADING_THREAD_INIT.call_once(|| {
			LOADING_THREAD = Some(Arc::new(Mutex::new(OffloadedFileLoader::new(queue))));
		});
	}
}


#[cfg(test)]
mod tests {
	use crate::render_server::RenderServer;
	use crate::{common::math_types::{Quaternion, Vector3}, camera::render_camera::RenderCamera};
	use vulkano::instance::Instance;
	use super::{LoadError, request_asset, Asset};
	use winit::platform::unix::EventLoopExtUnix;
	use std::time::Instant;

	#[test]
	// This test won't compile outside of unix since I'm relying on `EventLoopExtUnix`, as
	// tests are not run on the main thread and EventLoop::new() insists on cross-platform compatibility
	// including with iOS, which apparently does not support off-main creation of the event loop.
	//TODO: Make the request for an event loop use a function that compile-time chooses the right platform bc FUCK IOS ANYWAYS.
	// I'm not dragging my code down so I can support that garbage platform. 
	fn test_loading_texture() -> Result<(), LoadError> {
		let instance = {
			let extensions = vulkano_win::required_extensions();
			Instance::new(None, &extensions, None).expect("failed to create Vulkan instance")
		};
		let el = EventLoopExtUnix::new_any_thread();
		let (mut rs, ..) = RenderServer::new(instance, &el);
		rs.add_camera(Box::new(RenderCamera::new(Vector3::new(0.0, 0.0, 0.0), Quaternion::new(1.0, 0.0, 0.0, 0.0)).0));
		let _= request_asset(super::AssetRequest::Texture("triangle.png", true))?;
		let start = Instant::now();
		let mut texture = None;
		let mut frames_to_wait = 50;
		loop {
			if start.elapsed().as_secs_f64() > 10.0 { 
				panic!("Failed to load texture after ten seconds");
			}
			if texture.is_none() {
				if let Some(asset) = request_asset(super::AssetRequest::Texture("triangle.png", true))? {
					if let Asset::Texture(tex) = asset {
						texture = Some(tex);
					}
					println!("Successfully loaded the texture onto the GPU");
				}
			} else {
				if frames_to_wait == 0 {
					return Ok(());
				} else {
					if request_asset(super::AssetRequest::Texture("triangle.png", true))?.is_some() {
						println!("invariant upheld on iteration {}", 51 - frames_to_wait);
					} else {
						panic!("Asset went out of scope with valid reference?!?!")
					}
					frames_to_wait -= 1;
				}
			}
			rs.draw_frame(0.166666);
		}
	}
}