use crate::{
	common::{math::default_quat, math_types::*},
	asset_loading::mesh_instance::RenderObjectMessage,
	physics::{physics_object::PhysicsObjectMessage, physics_server::PhysicsServerMessage},
	render_server::message::RenderServerMessage,
};

use physics_camera::PhysicsCamera;
use render_camera::RenderCamera;
use std::sync::mpsc::Sender;

mod physics_camera;
pub mod render_camera;

#[allow(dead_code)]
pub struct Camera {
	render_sender: Sender<RenderObjectMessage>,
	phy_sender: Sender<PhysicsObjectMessage>,
}
impl Camera {
	pub fn new(
		render_server_sender: &Sender<RenderServerMessage>,
		physics_server_sender: &Sender<PhysicsServerMessage>,
		position: Vector3,
		rotation_o: Option<Quaternion>,
	) -> Self {
		let rotation = rotation_o.unwrap_or(default_quat());

		let (ren_cam, render_sender) = RenderCamera::new(position, rotation);
		let (phy_cam, phy_sender) =
			PhysicsCamera::new(render_sender.clone(), position, Quaternion::from(rotation));
		render_server_sender
			.send(RenderServerMessage::AddCamera(Box::new(ren_cam)))
			.expect("Failed to add camera");
		physics_server_sender
			.send(PhysicsServerMessage::AddObject(Box::new(phy_cam)))
			.expect("Failed to add camera to physics");

		Self {
			render_sender,
			phy_sender,
		}
	}
}

#[test]
fn check_for_parity() {
	use cgmath::Euler;
	let e = Euler::new(cgmath::Rad(3.0), cgmath::Rad(1.0), cgmath::Rad(0.0));
	let e_2 = Euler::from(Quaternion::from(e));
	assert!((e.z.0 - e_2.z.0).abs() < 0.0001);
}
